with import <nixpkgs> {};

{
  num_jobs ? "1000",
}:

let
  rivet_ana_name = "STAR_2013_DIJET";
  params = {
    order = "NLO";
    sqrts = "510";
    etmin = "17";
    pol_pdf_name = "DSSV_2014_NLO";
    unpol_pdf_name = "MSTW2008nlo68cl";
    inherit rivet_ana_name num_jobs;
  };
  fac = xren: xfac: import ./a_ll.nix (params // { inherit xren xfac; });
  member = m: import ./a_ll.nix (params // { pol_member = m; });
  scale_uncertainty_file = "${import ./scale_uncertainty.nix {inherit fac;}}/result.yoda";
  myyoda = yoda.overrideDerivation (old: { propagatedBuildInputs = old.propagatedBuildInputs ++ [ root ];
  postInstall = ''
    for prog in "$out"/bin/*; do
      wrapProgram "$prog" --set PYTHONPATH \$PYTHONPATH:$(toPythonPath "$out")
    done
  '';
});
in
stdenv.mkDerivation {
  name = "plot";
  buildInputs = [
    rivet
    ghostscript
root
  ];
  phases = [ "buildPhase" ];
  buildPhase = ''
    export RIVET_PLOT_PATH="${./info_all}"
    rivet-mkhtml \
      --single \
      --outputdir $out \
      --no-ratio \
      ${import ./a_ll.nix params}:"DSSV2014 / MSTW2008"
      PLOT:LogY=0:LegendXPos=0.2
  '';
}
#      ${scale_uncertainty_file}:"scale uncertainty":ErrorBands=1:ErrorBandColor=green:ErrorBandOpacity=0.5 \
#    cp ${scale_uncertainty_file} $out/scale_uncertainty.yoda
#pushd ${root}
#export MANPATH=/tmp/
#source bin/thisroot.sh
#popd
#env | grep PYTHON
#    ${myyoda}/bin/yoda2root $out/scale_uncertainty.yoda $out/scale_uncertainty.root
#  '';
#}
