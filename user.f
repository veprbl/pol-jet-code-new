c-----------------------------------------------------------------------
c **************************** USER ROUTINES ***************************
c-----------------------------------------------------------------------
C
C USUALLY IT IS ENOUGH TO MAKE MODIFICATIONS IN "INIJET" AND "OUTFUN" 
C SCALES DEFINE FACTORIZATION/RENORMALIZATION SCALES, THE DEFAULT IS DIJET INVARIANT MASS
C
C Also check jetalg if you want to change the jet definition (default
c  is anti-Kt with R=0.4 )

      subroutine inijet
c DEFINE HERE THE HISTOGRAMS TO BE FILLED IN THE "OUTFUN"
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14160E0, mxbin=100)
      common/shadr/sh
      ptmax=70.e0
      ptmin=0.e0
      np=35
      xmmax=120
      xmmin=16
      nx=11
      nx2=22
      
      emin=-1.9
      emax=3.2
      ne2=17
      neta=15
      

      ne1=13
      ne2=26

      emin1=-0.4
      emax1=2.2

      emin1=-2.2
      emax1=2.2
   


      cmin=0
      cmax=1
      cmax2=0.5
      nphi=20
      nphi2=10
      
      nxbin=24
      nxbin2=48
      xmin=-.1
      xmax=1.1

      blo=19
      bhi=23

c Reset histograms

      call inihist

c Here book the needed histograms, using:
c call  bookplot(hist. number, title, num. of bins, xmin, xmax, ....
c              ... xtitle, ytitle, yscale, nrep)
c  where yscale can be either   'LOG'
c                         or    'LIN'
c and for nrep=1  a new figure is created
c whereas for nrep=2 the hist. is included in the figure defined before. 
c
c

C INVARIANT MASS HISTOGRAMS WITH DIFFERENT BIN SIZES           
      call bookplot(1,'inv mass',nx,xmmin,xmmax,'M','mub/bin','LOG',1)
c      call bookplot(2,'inv mass',nx2,xmmin,xmmax,'M','mub/bin','LOG',1)

C RAPIDITY HISTOGRAMS WITH DIFFERENT BIN SIZES           
c      call bookplot(3,'eta1+eta2',ne1,emin1,emax1,
c     # 'eta1+eta2','mub/bin','LOG',1)
c      call bookplot(4,'eta1+eta2',ne2,emin1,emax1,
c     #       'eta1+eta2','mub/bin','LOG',1)
     
C COSTHETA HISTOGRAMS WITH DIFFERENT BIN SIZES           
c      call bookplot(5,'costheta*',nphi,cmin,cmax,'costheta*',
c     # 'mub/bin','LOG',1)
c      call bookplot(6,'costheta*',nphi2,cmin,cmax2,'costheta*',
c     # 'mub/bin','LOG',1)

C X1, X2
      call bookplot(7,'lx1',90,-3.d0,0.d0,'x1','mub/bin','LOG',1)
      call bookplot(8,'lx2',90,-3.d0,0.d0,'x2','mub/bin','LOG',1)

      return
      end


      subroutine outfun(www,iout)
C DEFINE KINEMATICAL VARIABLES AND FILL THE HISTOGRAMS
C WWW IS THE WEIGHT OF EACH EVENT THAT WILL BE USED TO FILL THE HISTOGRAMS
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      parameter (tiny=1.d-3)
      parameter (rcone=1.d0)
      parameter (des=1.d0)
      common/fixvar/xsc_min2,xlam,xmuf2,xmur2,xmues2,xmuww2,zg,ze2
      common/cmkin/xkt_cm,xeta_cm,xphi_cm
      common/shadr/sh
      common/imtt/imtt
      common/imtt2/imtt2
      common/x1x2/ycm,tau
      common/dirref/idirref
      dimension xkt_cm(1:3),xeta_cm(1:3),xphi_cm(1:3)
      dimension ykt(1:3),yeta(1:3),yphi(1:3)
      dimension zkt(1:3),zeta(1:3),zphi(1:3)
      integer imtt(3:4,3:5)
      integer imtt2(3:5,3:5),ialg

      call rivetout(www)

c set iout=0 to avoid counting event if doesn't fulfill cuts
c current rivetout implementation depends on iout to be one so that
c vegas integrates "full" cross-section that we then supply to Rivet
      iout=1
C          
c Collect "partonic" kinematics
      e = sqrt(sh)
      et_ev=0.d0
      do i=1,3
        ykt(i)=xkt_cm(i)
        yeta(i)=xeta_cm(i)
        yphi(i)=xphi_cm(i)
        et_ev=et_ev+ykt(i)
      enddo
c

c Call jet algorithm to define jets: jets kinematics now in zkt,zeta,zphi
c Jet algorithm defined in jetalg routine
c Call jet algorithm to define jets: jets kinematics now in zkt,zeta,zphi
      ialg=1
      call jetalg(ykt,yeta,yphi,zkt,zeta,zphi) 
      
      
c Order jets according to pT      
      xmax=max(zkt(1),zkt(2),zkt(3))
      xmin=min(zkt(1),zkt(2),zkt(3))
      do i=1,3
        if(zkt(i).eq.xmax)n1=i
        if(zkt(i).eq.xmin)n3=i
      enddo
      n2=imtt2(n1+2,n3+2)-2
c c now et(n3)<=et(n2)<=et(n1)  HARDEST JET IS N1, SECOND HARDEST JET IS N2
C THEREFORE  ZKT(N1) IS THE TRANSVERSE MOMENTUM OF THE HARDEST JET
C            ZETA(N1) IS THE RAPIDITY OF THE HARDEST JET 
C            ZPHI(N1) IS THE AZIMUTHAL ANGLE OF THE HARDEST JET
C AND SO ON..

C Define kinematical variables
        xone=( zkt(n1)*exp( zeta(n1))+zkt(n2)*exp( zeta(n2)) )/e-tiny
        xtwo=( zkt(n1)*exp(-zeta(n1))+zkt(n2)*exp(-zeta(n2)) )/e-tiny
        cs12=cos(zphi(n1)-zphi(n2))
        dphi=dacos(cs12)
        pl1=zkt(n1)*sinh(zeta(n1))
        pl2=zkt(n2)*sinh(zeta(n2))
        e1=zkt(n1)*cosh(zeta(n1))
        e2=zkt(n2)*cosh(zeta(n2))
C INVARIANT MASS OF HARDEST DIJETS        
        xm2=2*(e1*e2-pl1*pl2-zkt(n1)*zkt(n2)*cs12)
        xmjj=sqrt(xm2)        
        ptjj2=zkt(n1)**2+zkt(n2)**2+2*zkt(n1)*zkt(n2)*cs12
        ptjj=sqrt(ptjj2)
        deta=abs(zeta(n1)-zeta(n2))
C COSTHETA BETWEEN HARDEST DIJETS        
        costheta=tanh(deta/2.)
        etajj=0.5*log((e1+e2+pl1+pl2)/(e1+e2-pl1-pl2))


C SOME CUT PARAMETERS 
c           ptmin1=10d0
c           ptmin2=7d0
c           etamin= -1d0
c           etamax= 1d0
        ptmin1=8d0
        ptmin2=6d0
        etamin= -0.8d0
        etamax= 0.8d0

c SET CUTS HERE           
c double differential, cuts1
      if( zkt(n1).ge.ptmin1 .and. zeta(n1).le.etamax .and. 
     #   zeta(n1).ge.etamin .and. zkt(n2).ge.ptmin2 .and. 
     #   zeta(n2).le.etamax .and. zeta(n2).ge.etamin  
     #     .and. dphi.ge.2.09    
     #      ) then

c Last Condition was dphi.gt.2d0 (BP)

c set iout=1 to count event, do not change!
        iout=1         
C KEEP IOUT=1 WHEN CUTS ARE FULLFILLED
c

c To fill the histograms, use
c topfill(n,x,weight)
c where:
c n = histogram number
c x = x value
c weight = weight of the event (WWW IS THE WEIGHT IN THIS ROUTINE)

c invariant mass
        call topfill(1,xmjj,www )
C        call topfill(2,xmjj,www )

c eta two hardest jets
c eta1+eta2
c        call topfill(3,zeta(n1)+zeta(n2),www  )
c       call topfill(4,zeta(n1)+zeta(n2),www )

c costheta* 
c        call topfill(5,costheta,www )
c        call topfill(6,costheta,www  )
 
c x1,x2
        x1=zkt(n1)/sqrt(sh)*(exp(zeta(n1)) + exp(zeta(n2)))
        x2=zkt(n1)/sqrt(sh)*(exp(-zeta(n1)) + exp(-zeta(n2)))
c Works for LO. Should give similar result:
c        xx1=sqrt(tau) * exp(ycm)
c        xx2=tau/x1
c        x1=min(xx1,xx2)
c        x2=max(xx1,xx2)
        call topfill(7,log10(x1),www )
        call topfill(8,log10(x2),www )

       endif
      end


c      SUBROUTINE SCALES(xmu)
cc define renormalization and factorization default scales 
cc Define the reference scale in terms of these quantities
cc ONLY WITH A BOOST-INVARIANT PRESCRIPTION since
cc one can only access here the jet kinematics in the partonic CM frame
cc Something like the invariant mass or the transverse momentum of
cc the jet is a safe definition
c      implicit real * 8 (a-h,o-z)
c      common/partkin/xkt,xeta,xphi
c      common/imtt/imtt
c      common/imtt2/imtt2
c      dimension xkt(3:6),xeta(3:6),xphi(3:6)
c      dimension ykt(1:3),yeta(1:3),yphi(1:3)
c      dimension zkt(1:3),zeta(1:3),zphi(1:3)
c      integer imtt(3:4,3:5)
c      integer imtt2(3:5,3:5),ialg 
c      
cc transform into jet format (but still on partonic CM frame!)
c      do i=1,3
c        ykt(i)=xkt(i+2)
c        yeta(i)=xeta(i+2)
c        yphi(i)=xphi(i+2)
c      enddo
c
c
cc Call jet algorithm to define jets: jets kinematics now in zkt,zeta,zphi
cc Jet algorithm defined in jetalg routine
cc Call jet algorithm to define jets: jets kinematics now in zkt,zeta,zphi
c      ialg=1
c      call jetalg(ykt,yeta,yphi,zkt,zeta,zphi) 
c
cc Order jets according to pT      
c      xmax=max(zkt(1),zkt(2),zkt(3))
c      xmin=min(zkt(1),zkt(2),zkt(3))
c      do i=1,3
c        if(zkt(i).eq.xmax)n1=i
c        if(zkt(i).eq.xmin)n3=i
c      enddo
c      n2=imtt2(n1+2,n3+2)-2
cc c now et(n3)<=et(n2)<=et(n1)
c 
cC Define kinematical variables
c        cs12=cos(zphi(n1)-zphi(n2))
c        dphi=dacos(cs12)
c        pl1=zkt(n1)*sinh(zeta(n1))
c        pl2=zkt(n2)*sinh(zeta(n2))
c        e1=zkt(n1)*cosh(zeta(n1))
c        e2=zkt(n2)*cosh(zeta(n2))
c        
cc invariant mass 
c        xm2=2*(e1*e2-pl1*pl2-zkt(n1)*zkt(n2)*cs12)
c        xmjj=sqrt(xm2)
cc pt of the hardest jet
c         pthard=zkt(n1)
cc pt of the two hardest jet
c         pthard_two=(zkt(n1)+zkt(n2))
cc total pt of the jets
c         pttotal=(zkt(n1)+zkt(n2)+zkt(n3))
c
cc definition of default scale : INVARIANT MASS
c         xmu=xmjj
cc         
c         return
c         end







c-----------------------------------------------------------------------
c ********************* JET-FINDING ROUTINES ***************************
c-----------------------------------------------------------------------
c
      subroutine jetalg(ykt,yeta,yphi,zkt,zeta,zphi)
      implicit real * 8 (a-h,o-z)
      integer ialg 
      dimension ykt(1:3),yeta(1:3),yphi(1:3)
      dimension zkt(1:3),zeta(1:3),zphi(1:3)
c      
       R=0.6d0  ! jet parameter
       ialg =1  ! jet algorithm definition
c     ialg=1  antikt with E-recombination    
c     ialg=2  kt with PT weighted recombination      
c     ialg=3  cone with Rsep=1.3 and E-recombination
c Always check jet algorithm definition since recombination (merging)
c  schemes can differ from the one used at a given experiment
      if (ialg.eq.1) then
          call antikt(ykt,yeta,yphi,zkt,zeta,zphi,R)       
      elseif (ialg.eq.2) then 
          call esjet(ykt,yeta,yphi,zkt,zeta,zphi,R)
      elseif (ialg.eq.3) then 
          call cone2(ykt,yeta,yphi,zkt,zeta,zphi,R)
      else 
          write(6,*)'Wrong jet algorithm parameter ialg= ',ialg
          stop
      endif
      end
      
      
c
c Cone algorithm
c
      subroutine cone(ykt,yeta,yphi,zkt,zeta,zphi,r_cone)
c This subroutine returns jet variables (zkt, zeta, zphi) defined
c in terms of partonic variables (ykt, yeta, yphi) in the cone
c algorithm. 
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      common/rcone/rcone
      common/imtt2/imtt2
      dimension ykt(1:3),yeta(1:3),yphi(1:3)
      dimension zkt(1:3),zeta(1:3),zphi(1:3)
      dimension xr(1:2,2:3),xp(1:2,2:3)
      integer imtt2(3:5,3:5)
c
      rcone=r_cone
      
      i2bd=1
      do i=1,3
        if(ykt(i).eq.0.d0)i2bd=0
      enddo
      if(i2bd.eq.0)then
c 2-body kinematics
        do i=1,3
          zkt(i)=ykt(i)
          zeta(i)=yeta(i)
          zphi(i)=yphi(i)
        enddo
      else
c 3-body kinematics
        xr(1,2)=rdist(yeta(1),yphi(1),yeta(2),yphi(2))
        xp(1,2)=ptdist(ykt(1),ykt(2))
        xr(1,3)=rdist(yeta(1),yphi(1),yeta(3),yphi(3))
        xp(1,3)=ptdist(ykt(1),ykt(3))
        xr(2,3)=rdist(yeta(2),yphi(2),yeta(3),yphi(3))
        xp(2,3)=ptdist(ykt(2),ykt(3))
        imerge=0
        do i=1,2
          do j=i+1,3
            if(xr(i,j).lt.xp(i,j))then
              if(imerge.eq.1)then
                write(6,*)'Fatal error in subroutine cone'
                stop
              endif
              imerge=1
              n1=i
              n2=j
            endif
          enddo
        enddo
        if(imerge.eq.0)then
c no merging
          do i=1,3
            zkt(i)=ykt(i)
            zeta(i)=yeta(i)
            zphi(i)=yphi(i)
          enddo
        else
          n3=imtt2(n1+2,n2+2)-2
          zkt(n3)=ykt(n3)
          zeta(n3)=yeta(n3)
          zphi(n3)=yphi(n3)
          call xmerge(ykt(n1),ykt(n2),tmpkt,
     #                yeta(n1),yeta(n2),tmpeta,
     #                yphi(n1),yphi(n2),tmpphi)
          zkt(n1)=tmpkt
          zeta(n1)=tmpeta
          zphi(n1)=tmpphi
          zkt(n2)=0.d0
          zeta(n2)=1.d8
          zphi(n2)=0.d0
        endif
      endif
      return
      end





c Cone algorithm
c
      subroutine cone2(ykt,yeta,yphi,zkt,zeta,zphi,r_cone)
C MODIFIED CONE TO INCLUDE PARAMETER RSEP      
c This subroutine returns jet variables (zkt, zeta, zphi) defined
c in terms of partonic variables (ykt, yeta, yphi) in the cone
c algorithm. 
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      common/rcone/rcone
      common/imtt2/imtt2
      dimension ykt(1:3),yeta(1:3),yphi(1:3)
      dimension zkt(1:3),zeta(1:3),zphi(1:3)
      dimension xr(1:2,2:3),xp(1:2,2:3)
      integer imtt2(3:5,3:5)
c
      rcone=r_cone
      i2bd=1
      do i=1,3
        if(ykt(i).eq.0.d0)i2bd=0
      enddo
      if(i2bd.eq.0)then
c 2-body kinematics
        do i=1,3
          zkt(i)=ykt(i)
          zeta(i)=yeta(i)
          zphi(i)=yphi(i)
        enddo
      else
c 3-body kinematics
        xr(1,2)=rdist(yeta(1),yphi(1),yeta(2),yphi(2))
        xp(1,2)=ptdist(ykt(1),ykt(2))
        xr(1,3)=rdist(yeta(1),yphi(1),yeta(3),yphi(3))
        xp(1,3)=ptdist(ykt(1),ykt(3))
        xr(2,3)=rdist(yeta(2),yphi(2),yeta(3),yphi(3))
        xp(2,3)=ptdist(ykt(2),ykt(3))
        imerge=0
        do i=1,2
          do j=i+1,3
            if(xr(i,j).lt.xp(i,j))then
              if(imerge.eq.1)then
                write(6,*)'Fatal error in subroutine cone'
                stop
              endif
              imerge=1
              n1=i
              n2=j
            endif
          enddo
        enddo


c introduce Rsep=1.3
        if (imerge.eq.1) then
            rsep=1.3*rcone
            if (xr(n1,n2).gt.rsep) imerge=0
        endif 


        if(imerge.eq.0)then
c no merging
          do i=1,3
            zkt(i)=ykt(i)
            zeta(i)=yeta(i)
            zphi(i)=yphi(i)
          enddo
        else
          n3=imtt2(n1+2,n2+2)-2
          zkt(n3)=ykt(n3)
          zeta(n3)=yeta(n3)
          zphi(n3)=yphi(n3)
c          call xmerge(ykt(n1),ykt(n2),tmpkt,
c     #                yeta(n1),yeta(n2),tmpeta,
c     #                yphi(n1),yphi(n2),tmpphi)
          call xmerge2(ykt(n1),ykt(n2),tmpkt,
     #                yeta(n1),yeta(n2),tmpeta,
     #                yphi(n1),yphi(n2),tmpphi)
           zkt(n1)=tmpkt
          zeta(n1)=tmpeta
          zphi(n1)=tmpphi
          zkt(n2)=0.d0
          zeta(n2)=1.d8
          zphi(n2)=0.d0
        endif
      endif
      return
      end


      function rdist(eta1,phi1,eta2,phi2)
c
c Square root of eq. (2.22)
c
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
c
      dteta=eta1-eta2
      dtphi=phi1-phi2
      dtphi=dacos(dcos(dtphi))
      rdist=dsqrt(dteta**2+dtphi**2)
      return
      end


      function ptdist(pt1,pt2)
c
c (pt1+pt2)*rcone/max(pt1,pt2), for the cone algorithm
c
      implicit real * 8 (a-h,o-z)
      common/rcone/rcone
c
      tmp=(pt1+pt2)/max(pt1,pt2)
      ptdist=tmp*rcone
      return
      end
c
c End of the cone algorithm
c
c 
c Ellis-Soper algorithm
c
      subroutine esjet(ykt,yeta,yphi,zkt,zeta,zphi,d_jet)
c This subroutine returns jet variables (zkt, zeta, zphi) defined
c in terms of partonic variables (ykt, yeta, yphi) in the Ellis-Soper
c algorithm. The parameter D must be set in the function ddist (in this file)
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      common/djet/djet
      common/imtt/imtt
      common/imtt2/imtt2
      dimension ykt(1:3),yeta(1:3),yphi(1:3)
      dimension zkt(1:3),zeta(1:3),zphi(1:3)
      dimension xd(1:3),xd2(1:2,2:3)
      integer imtt(3:4,3:5)
      integer imtt2(3:5,3:5)
c
      djet=d_jet
      i2bd=1
      do i=1,3
        if(ykt(i).eq.0.d0)i2bd=0
      enddo
      if(i2bd.eq.0)then
c 2-body kinematics
        do i=1,3
          zkt(i)=ykt(i)
          zeta(i)=yeta(i)
          zphi(i)=yphi(i)
        enddo
      else
c 3-body kinematics
        xd(1)=ykt(1)**2
        xd(2)=ykt(2)**2
        xd(3)=ykt(3)**2
        xd2(1,2)=ddist(ykt(1),yeta(1),yphi(1),ykt(2),yeta(2),yphi(2))
        xd2(1,3)=ddist(ykt(1),yeta(1),yphi(1),ykt(3),yeta(3),yphi(3))
        xd2(2,3)=ddist(ykt(2),yeta(2),yphi(2),ykt(3),yeta(3),yphi(3))
        xmin=min(xd(1),xd(2),xd(3),xd2(1,2),xd2(1,3),xd2(2,3))
        imerge=-1
        do i=1,3
          if(xd(i).eq.xmin)then
            imerge=0
            n1=i
          endif
        enddo
        if(imerge.eq.-1)then
          do i=1,2
            do j=i+1,3
              if(xd2(i,j).eq.xmin)then
                imerge=1
                n1=i
                n2=j
              endif
            enddo
          enddo
        endif
        if(imerge.eq.0)then
c no merging
          n2=imtt(3,n1+2)-2
          n3=imtt(4,n1+2)-2
          if(xd2(n2,n3).lt.min(xd(n2),xd(n3)))then
            write(6,*)'This is S2 contribution'
            stop
          endif
          do i=1,3
            zkt(i)=ykt(i)
            zeta(i)=yeta(i)
            zphi(i)=yphi(i)
          enddo
        elseif(imerge.eq.1)then
          n3=imtt2(n1+2,n2+2)-2
          zkt(n3)=ykt(n3)
          zeta(n3)=yeta(n3)
          zphi(n3)=yphi(n3)
          call xmerge(ykt(n1),ykt(n2),tmpkt,
     #                yeta(n1),yeta(n2),tmpeta,
     #                yphi(n1),yphi(n2),tmpphi)
          zkt(n1)=tmpkt
          zeta(n1)=tmpeta
          zphi(n1)=tmpphi
          zkt(n2)=0.d0
          zeta(n2)=1.d8
          zphi(n2)=0.d0
        else
          write(6,*)'Fatal error in subroutine esjet'
          write(6,*)'imerge=',imerge
          stop
        endif
      endif
      return
      end


      function rdist2(eta1,phi1,eta2,phi2)
c
c Eq. (2.22)
c
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
c
      dteta=eta1-eta2
      dtphi=phi1-phi2
      dtphi=dacos(dcos(dtphi))
      rdist2=dteta**2+dtphi**2
      return
      end


      function ddist(pt1,eta1,phi1,pt2,eta2,phi2)
c
c Eq. (2.23)
c
      implicit real * 8 (a-h,o-z)
      common/djet/djet
c
      tmp=min(pt1**2,pt2**2)*rdist2(eta1,phi1,eta2,phi2)
      ddist=tmp/djet**2
      return
      end
c
c End of the Ellis-Soper algorithm
c
      subroutine xmerge
     #    (xkt1,xkt2,xkt3,xeta1,xeta2,xeta3,xphi1,xphi2,xphi3)
c
c Defines the jet variables (xkt3,xeta3,xphi3) in terms of the
c partonic variables when two partons are merged
c     PT weighted recombination scheme
c
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
c
      xkt3=xkt1+xkt2
      xeta3=(xkt1*xeta1+xkt2*xeta2)/xkt3
      if(abs(xphi1-xphi2).lt.pi)then
        xphi3=(xkt1*xphi1+xkt2*xphi2)/xkt3
      else
        if(xphi2.lt.xphi1)then
          xphi3=(xkt1*xphi1+xkt2*(xphi2+2*pi))/xkt3
        else
          xphi3=(xkt1*xphi1+xkt2*(xphi2-2*pi))/xkt3
        endif
      endif
      xphi3=atan2(sin(xphi3),cos(xphi3))
      return
      end

c

      subroutine xmerge2
     #    (xkt1,xkt2,xkt3,xeta1,xeta2,xeta3,xphi1,xphi2,xphi3)
c
c Defines the jet variables (xkt3,xeta3,xphi3) in terms of the
c partonic variables when two partons are merged
c  Recombination in E-scheme for merging
c  xeta3= true rapidity (not pseudorapidity)
c
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
c
c energy and momentum of parton 1
      c1 = (exp(2*xeta1)-1)/(exp(2*xeta1)+1)
      e1 = xkt1/(1-c1**2)**0.5
      px1 = xkt1 * Cos(xphi1)
      py1 = xkt1 * sin(xphi1)
      pz1 =  e1 * c1
c energy and momentum of parton 2
      c2 = (exp(2*xeta2)-1)/(exp(2*xeta2)+1)
      e2 = xkt2/(1-c2**2)**0.5
      px2 = xkt2 * Cos(xphi2)
      py2 = xkt2 * sin(xphi2)
      pz2 =  e2 * c2
c energy and momentum of parton 1+2
      e3 = e1 + e2
      px3 = px1 + px2
      py3 = py1 + py2
      pz3 = pz1 + pz2
c phi,eta and Et for merged jet
      xphi3 = atan2(py3,px3)
      xeta3 = 0.5d0 * log( (e3+pz3)/(e3-pz3)) !y = rapidity
      pmod=(pz3**2 + px3**2 + py3**2)**0.5
c      xeta3= 0.5d0 * log( (pmod+pz3)/(pmod-pz3)) ! eta pseudorapidity
      xkt3=(px3**2+py3**2)**0.5
c
      return
      end

c 
c anti-kt algorithm
c
      subroutine antikt(ykt,yeta,yphi,zkt,zeta,zphi,d_jet)
c This subroutine returns jet variables (zkt, zeta, zphi) defined
c in terms of partonic variables (ykt, yeta, yphi) in the Ellis-Soper
c algorithm. The parameter D must be set in the function ddist (in this file)
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      common/djet/djet
      common/imtt/imtt
      common/imtt2/imtt2
      dimension ykt(1:3),yeta(1:3),yphi(1:3)
      dimension zkt(1:3),zeta(1:3),zphi(1:3)
      dimension xd(1:3),xd2(1:2,2:3)
      integer imtt(3:4,3:5)
      integer imtt2(3:5,3:5)
c
      djet=d_jet
      i2bd=1
      do i=1,3
        if(ykt(i).eq.0.d0)i2bd=0
      enddo
      if(i2bd.eq.0)then
c 2-body kinematics
        do i=1,3
          zkt(i)=ykt(i)
          zeta(i)=yeta(i)
          zphi(i)=yphi(i)
        enddo
      else
c 3-body kinematics
        xd(1)=1d0/ykt(1)**2
        xd(2)=1d0/ykt(2)**2
        xd(3)=1d0/ykt(3)**2
        xd2(1,2)=adist(ykt(1),yeta(1),yphi(1),ykt(2),yeta(2),yphi(2))
        xd2(1,3)=adist(ykt(1),yeta(1),yphi(1),ykt(3),yeta(3),yphi(3))
        xd2(2,3)=adist(ykt(2),yeta(2),yphi(2),ykt(3),yeta(3),yphi(3))
        xmin=min(xd(1),xd(2),xd(3),xd2(1,2),xd2(1,3),xd2(2,3))
c        write(6,*)"xd(1),xd(2),xd(3),xd2(1,2),xd2(1,3),xd2(2,3),xmin"
c        write(6,*)xd(1),xd(2),xd(3),xd2(1,2),xd2(1,3),xd2(2,3),xmin
        imerge=-1
        do i=1,3
          if(xd(i).eq.xmin)then
            imerge=0
            n1=i
          endif
        enddo
        if(imerge.eq.-1)then
          do i=1,2
            do j=i+1,3
              if(xd2(i,j).eq.xmin)then
                imerge=1
                n1=i
                n2=j
              endif
            enddo
          enddo
        endif
        
        if(imerge.eq.0)then
c no merging yet eliminate n1 from the list (n1 = jet)
          n2=imtt(3,n1+2)-2
          n3=imtt(4,n1+2)-2
            zkt(n1)=ykt(n1)
            zeta(n1)=yeta(n1)
            zphi(n1)=yphi(n1)
c Now check if merging needed between n2 and n3            
          if(xd2(n2,n3).lt.min(xd(n2),xd(n3)))then
c merging of n2 and n3            
          call xmerge2(ykt(n3),ykt(n2),tmpkt,
     #                yeta(n3),yeta(n2),tmpeta,
     #                yphi(n3),yphi(n2),tmpphi)
          zkt(n2)=tmpkt
          zeta(n2)=tmpeta
          zphi(n2)=tmpphi
          zkt(n3)=0.d0
          zeta(n3)=1.d8
          zphi(n3)=0.d0
c          write(6,*)n2,n3,n1,imerge
c          write(6,*)ykt(n2),ykt(n3),ykt(n1)
c          write(6,*)yeta(n2),yeta(n3),yeta(n1)
c          write(6,*)yphi(n2),yphi(n3),yphi(n1)
c          write(6,*)'cc'
c          write(6,*)zkt(n2),zkt(n3),zkt(n1)
c          write(6,*)zeta(n2),zeta(n3),zeta(n1)
c          write(6,*)zphi(n2),zphi(n3),zphi(n1)
c          write(6,*)zkt(n2)/zkt(n1)          
          else
c  no merging of n2 and n3
            zkt(n2)=ykt(n2)
            zeta(n2)=yeta(n2)
            zphi(n2)=yphi(n2)
            zkt(n3)=ykt(n3)
            zeta(n3)=yeta(n3)
            zphi(n3)=yphi(n3)
c          write(6,*) xd2(n2,n3), xd(n2),xd(n3)
c          write(6,*)'cc'
c          write(6,*)zkt(n2),zkt(n3),zkt(n1)
c          write(6,*)zeta(n2),zeta(n3),zeta(n1)
c          write(6,*)zphi(n2),zphi(n3),zphi(n1)
          endif
                              
        elseif(imerge.eq.1)then
c merge n1 and n2 (into the new n1), n3 is the other jet        
          n3=imtt2(n1+2,n2+2)-2
          zkt(n3)=ykt(n3)
          zeta(n3)=yeta(n3)
          zphi(n3)=yphi(n3)
          call xmerge2(ykt(n1),ykt(n2),tmpkt,
     #                yeta(n1),yeta(n2),tmpeta,
     #                yphi(n1),yphi(n2),tmpphi)
          zkt(n1)=tmpkt
          zeta(n1)=tmpeta
          zphi(n1)=tmpphi
          zkt(n2)=0.d0
          zeta(n2)=1.d8
          zphi(n2)=0.d0

c          write(6,*)n1,n2,n3,imerge
c          write(6,*)ykt(n1),ykt(n2),ykt(n3)
c          write(6,*)yeta(n1),yeta(n2),yeta(n3)
c          write(6,*)yphi(n1),yphi(n2),yphi(n3)
c          write(6,*)"vv"
c          write(6,*)zkt(n1),zkt(n2),zkt(n3)
c          write(6,*)zeta(n1),zeta(n2),zeta(n3)
c          write(6,*)zphi(n1),zphi(n2),zphi(n3)
c          write(6,*)
c check separation is fine, should not combine n1 and n3 now       
        xd22=adist(zkt(n1),zeta(n1),zphi(n1),zkt(n3),zeta(n3),zphi(n3))
        dminnew=min(xdd2,1/zkt(n1)**2,1/zkt(n3)**2)
           if (dminnew.eq.xd22) then
             write(6,*)'Fatal error in anti-kt : only 1 jet remains'
             write(6,*)xd22,1/zkt(n1)**2,1/zkt(n3)**2
             stop
           endif  
        else
          write(6,*)'Fatal error in subroutine antikt'
          write(6,*)'imerge=',imerge
          stop
        endif
      endif
      return
      end


      function adist(pt1,eta1,phi1,pt2,eta2,phi2)
      implicit real * 8 (a-h,o-z)
      common/djet/djet
c
      tmp=min(1d0/pt1**2,1d0/pt2**2)*rdist2(eta1,phi1,eta2,phi2)
      adist=tmp/djet**2
      return
      end
c
c End of anti-KT algorithm
c
