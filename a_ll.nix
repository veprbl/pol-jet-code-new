with import <nixpkgs> {};

{
  rivet_ana_name,
  sqrts,
  etmin,
  pol_pdf_name,
  pol_member ? 0,
  unpol_pdf_name,
  num_jobs ? "1000",
  order ? "NLO",
  xren ? "1.0",
  xfac ? "1.0",
}:

let
  pol = import ./default.nix {
    pdf_name = pol_pdf_name;
    member = pol_member;
    inherit rivet_ana_name sqrts etmin num_jobs order xren xfac;
  };
  unpol = import ./default.nix {
    pdf_name = unpol_pdf_name;
    member = 0;
    inherit rivet_ana_name sqrts etmin num_jobs order xren xfac;
  };
  yoda_utils = import ./yoda_utils.nix;
in
assert pol.polarized == "1";
assert unpol.polarized == "0";
stdenv.mkDerivation {
  name = "all--${pol.name}--${unpol.name}";
  phases = [ "buildPhase" ];
  buildPhase = ''
    mkdir $out
    yoda-ratio ${pol}/result.yoda ${unpol}/result.yoda $out/result.yoda
  '';

  buildInputs = [ yoda_utils pol unpol ];

  passthru = {
    inherit pol_pdf_name unpol_pdf_name;
  };
}
