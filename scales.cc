// vim:et sw=3 sts=3

#include <vector>

#include <fastjet/ClusterSequence.hh>

fastjet::JetDefinition jetdef(fastjet::antikt_algorithm, 0.6);

extern "C" {

extern struct {
   double xkt[4];
   double xeta[4];
   double xphi[4];
} partkin_;

void disable_scales_(double&);

void scales_(double &xmu)
{
   std::vector<fastjet::PseudoJet> partons;

   for(int i = 0; i < 3; i++)
   {
      // absence of particle is marked by kt=0
      // gap may happen at any index
      if (partkin_.xkt[i] == 0) continue;

      double p_x = partkin_.xkt[i] * cos(partkin_.xphi[i]);
      double p_y = partkin_.xkt[i] * sin(partkin_.xphi[i]);
      double p_z = partkin_.xkt[i] * sinh(partkin_.xeta[i]);
      double p = partkin_.xkt[i] * cosh(partkin_.xeta[i]);
      partons.push_back(fastjet::PseudoJet(p_x, p_y, p_z, p));
   }

   fastjet::ClusterSequence clust_seq(partons, jetdef);
   std::vector<fastjet::PseudoJet> jets = fastjet::sorted_by_pt(clust_seq.inclusive_jets());

   double mjj = (jets[0] + jets[1]).m();
   xmu = mjj;
}

}
