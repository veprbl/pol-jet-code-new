c**************************************************************************
c
c Classification of the partonic subprocesses:
c every process contributing to the physical cross section
c will be unambiguosly identified by the pair of integers
c
c                (jproc,jinst)
c
c Jproc identifies the unphysical matrix element 0 --> 5,
c while jinst identifies the initial state.
c We adopt the following conventions
c
c       jproc         unphysical process
c
c         1              0 --> 5g
c         2              0 --> 3g2q
c         3              0 --> 1g2q2Q
c         4              0 --> 1g4q
c
c
c       jinst         initial state
c
c         1                g g
c         2                q g
c         3                q qb
c         4                q q
c         5                q Qb
c         6                q Q
c
c
c We have therefore the following physical processes
c
c    (jproc,jinst)    physical process
c
c        (1,1)        g,g --> g,g,g
c
c        (2,1)        g,g --> q,qb,g
c        (2,2)        q,g --> q,g,g
c        (2,3)       q,qb --> g,g,g
c
c        (3,2)        q,g --> q,Q,Qb
c        (3,3)       q,qb --> Q,Qb,g
c        (3,5)       q,Qb --> q,Qb,g
c        (3,6)        q,Q --> q,Q,g
c
c        (4,2)        q,g --> q,q,qb
c        (4,3)       q,qb --> q,qb,g
c        (4,4)        q,q --> q,q,g
c
c 
c The leading order processes 0 --> 4 can be formally obtained
c from the 0 --> 5 ones by eliminating one gluon. We will mantain 
c the same classification scheme used for the 0 --> 5 processes. 
c Jproc=1 will mean the process 0 --> 4g, and so on.  The following 
c physical processes may occur in the leading order
c
c    (jproc,jinst)    physical process
c
c        (1,1)        g,g --> g,g
c
c        (2,1)        g,g --> q,qb
c        (2,2)        q,g --> q,g
c        (2,3)       q,qb --> g,g
c
c        (3,3)       q,qb --> Q,Qb
c        (3,5)       q,Qb --> q,Qb
c        (3,6)        q,Q --> q,Q
c
c        (4,3)       q,qb --> q,qb
c        (4,4)        q,q --> q,q
c
c**************************************************************************
c
c
c Begin of the 2 --> 3 routines
c
      function xmatel_five_part
     #     (s,xpp,xinv,xii,yi,xij,yj,jproc,jinst,ni,nj)
c This function, given the partonic CM energy (s) and the dot products of
c the four-momenta of the partons (xpp), returns the value of the 2 --> 3 
c matrix element (physical configuration) for the process specified 
c by (jproc,jinst)
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      parameter (tiny_s=1.d-7)
      parameter (tiny_c=1.d-8)
      parameter (izero=0)
      common/i_type_part/i_type_part
      common/i_ap_in/i_ap_in1,i_ap_in2
      common/i_ap_out/i_ap_out
      common/jcoll_prc/j_prc_1_coll,j_prc_2_coll
      common/jcoll_ins/j_ins_1_coll,j_ins_2_coll
      common/jcoll_out_prc/j_prc_coll_out
      common/polarized/lpol
      dimension xpp(1:5,1:5),ypp(1:5,1:5),xinv(1:5)
      integer i_type_part(1:4,1:6,3:5)
      integer i_ap_in1(1:4,1:6,3:5),i_ap_in2(1:4,1:6,3:5)
      integer i_ap_out(1:4,1:6,3:5,3:5)
      integer j_prc_1_coll(1:4,1:6,3:5),j_prc_2_coll(1:4,1:6,3:5)
      integer j_ins_1_coll(1:4,1:6,3:5),j_ins_2_coll(1:4,1:6,3:5)
      integer j_prc_coll_out(1:4,1:6,3:4,4:5)
c
      if(xii.lt.tiny_s)then
c Parton number ni is soft: use the analytical formula
c for the soft limit of the matrix element
        if(i_type_part(jproc,jinst,ni).ne.0)then
c The soft parton is not a gluon: the limit is finite, and the factor
c xii**2 in front of the matrix element gives therefore 0
          xmatel_five_part=0.d0
        else
c The soft parton is a gluon: use eq.(4.17)
          tmp=0.d0
          call x_xp_soft_yp(xpp,ypp,ni)
          neff=0
          do n=1,4
            if(n.ne.ni)then
              neff=neff+1
              meff=neff
            endif
            do m=n+1,5
              if(n.ne.ni.and.m.ne.ni)then
                meff=meff+1
                xfct1=eik_mni(xpp,xinv,yi,yj,n,m,ni,nj)
                xfct2=x_color_link(s,ypp,jproc,jinst,neff,meff)
                tmp=tmp+xfct1*xfct2
              endif
            enddo
          enddo
          xmatel_five_part=tmp/(8*pi**2)
        endif
      elseif(abs(1-yi).lt.tiny_c)then
c Parton number ni is collinear to parton 1: use the analytical formula
c for the collinear limit of the matrix element
        jproc0=j_prc_1_coll(jproc,jinst,ni)
        jinst0=j_ins_1_coll(jproc,jinst,ni)
        if(jproc0.eq.-1)then
c The splitting is not allowed: the limit is zero
          xmatel_five_part=0.d0
        else
c The splitting is allowed: use eq.(B.41)
          icode=i_ap_in1(jproc,jinst,ni)
c Momentum fraction entering the Altarelli-Parisi kernels
          x_ap=1-xii
c Reduced energy for the incoming partons
          s_red=s*x_ap
          xfct1=ap_kern(x_ap,abs(icode),lpol)
          call x_xp_collin_yp(s_red,xpp,ypp,ni,icode,1)
          xfct2=xmatel_four_part(s_red,ypp,jproc0,jinst0)
          xmatel_five_part=(4/s)*(1+yi)*xfct1*xfct2
        endif
      elseif(abs(1+yi).lt.tiny_c)then
c Parton number ni is collinear to parton 2: use the analytical formula
c for the collinear limit of the matrix element
        jproc0=j_prc_2_coll(jproc,jinst,ni)
        jinst0=j_ins_2_coll(jproc,jinst,ni)
        if(jproc0.eq.-1)then
c The splitting is not allowed: the limit is zero
          xmatel_five_part=0.d0
        else
c The splitting is allowed: use eq.(B.41)
          icode=i_ap_in2(jproc,jinst,ni)
c Momentum fraction entering the Altarelli-Parisi kernels
          x_ap=1-xii
c Reduced energy for the incoming partons
          s_red=s*x_ap
          xfct1=ap_kern(x_ap,abs(icode),lpol)
          call x_xp_collin_yp(s_red,xpp,ypp,ni,icode,-1)
          xfct2=xmatel_four_part(s_red,ypp,jproc0,jinst0)
          xmatel_five_part=(4/s)*(1-yi)*xfct1*xfct2
        endif
      elseif(abs(1-yj).lt.tiny_c)then
c Parton number ni is collinear to parton number nj: use the analytical 
c formula for the collinear limit of the matrix element
        n1=min(ni,nj)
        n2=max(ni,nj)
        jproc0=j_prc_coll_out(jproc,jinst,n1,n2)
        if(jproc0.eq.-1)then
c The splitting is not allowed: the limit is zero
          xmatel_five_part=0.d0
        else
c The splitting is allowed: use eq.(B.30)
          icode=i_ap_out(jproc,jinst,n1,n2)
          if(icode.eq.3.and.nj.lt.ni)then
            icode_ap=4
          else
            icode_ap=abs(icode)
          endif
c Momentum fraction entering the Altarelli-Parisi kernels
          x_ap=xij/(xii+xij)
          xfct1=ap_kern(x_ap,icode_ap,izero)
          call x_xp_collout_yp(xpp,ypp,n1,n2,icode)
          xfct2=xmatel_four_part(s,ypp,jproc0,jinst)
          xmatel_five_part=(4/s)*(1/x_ap)*xfct1*xfct2
        endif
      else
c The kinematical configuration is not in one of the singular regions:
c use the full expression for the matrix element
        if(yi.eq.2.d0)then
c Factor multiplying the matrix element, eq.(4.65)
c The factor xij is inserted in the main program
          xfact=(1-yj)*xii**2
        elseif(yj.eq.2.d0)then
c Factor multiplying the matrix element, eq.(4.37)
          xfact=(1-yi**2)*xii**2
        else
          write(6,*)'xmatel_five_part called in the wrong way'
          stop
        endif
c Get the dot products to calculate the relevant matrix element
        call xpp_to_ypp(xpp,ypp,jproc,jinst)
        if(jproc.eq.1)then
          tmp=p5_ggggg(ypp)
        elseif(jproc.eq.2)then
          tmp=p5_qqbggg(ypp,jinst)
        elseif(jproc.eq.3)then
          tmp=p5_qpqpg(ypp,jinst)
        elseif(jproc.eq.4)then
          tmp=p5_qqqqg(ypp,jinst)
        else
          write(6,*)'Error in xmatel_five_part: unknown process number' 
          stop
        endif
        tmp=tmp/x_average(jinst)
        xmatel_five_part=xfact*tmp/(2*s)
      endif
      return
      end


      subroutine xmatel_coll(s,xpp,xi_ev,xi,y,jproc,jinst,
     #          jproc0,jinst0,ni,xmtel,xmtel_sc)
c This function, given a collinear configuration (xpp), returns
c the corresponding 2 --> 2 matrix elements times a factor depending
c upon the Altarelli-Parisi kernels and the K functions (which allow
c for a change of the factorization scheme), matching the definition
c of eq.(5.7). xmtel_sc contains the contribution K^{(reg)} (regular
c part of the K function) when xi#0, and K^{(d)} (the part of the
c K function proportional to delta(xi)) when xi=0.
      implicit real * 8 (a-h,o-z)
      character * 2 scheme
      parameter (pi=3.14159265358979312D0)
      parameter (one=1.d0)
      common/fixvar/xsc_min2,xlam,xmuf2,xmur2,xmues2,xmuww2,zg,ze2
      common/cutpar/xicut,yincut,youtcut
      common/i_ap_in/i_ap_in1,i_ap_in2
      common/scheme/scheme
      common/polarized/lpol
      dimension xpp(1:5,1:5),ypp(1:5,1:5)
      integer i_ap_in1(1:4,1:6,3:5),i_ap_in2(1:4,1:6,3:5)
c
c Momentum fraction entering the Altarelli-Parisi kernels
      x_ap=1-xi
c Reduced energy for the incoming partons
      s_red=s*x_ap
      if(y.eq.1.d0)then
c Parton number ni is collinear to parton number 1
        icode=i_ap_in1(jproc,jinst,ni)
        call x_xp_collin_yp(s_red,xpp,ypp,ni,icode,1)
      elseif(y.eq.-1.d0)then
c Parton number ni is collinear to parton number 2
        icode=i_ap_in2(jproc,jinst,ni)
        call x_xp_collin_yp(s_red,xpp,ypp,ni,icode,-1)
      else
        write(6,*)'error in xmatel_coll: wrong y value'
        stop
      endif
      xlgsdi_mu=log((s*yincut)/(2*xmuf2))
      xfct1=ap_kern(x_ap,abs(icode),lpol)
      xfct2=apprime_kern(x_ap,abs(icode),lpol)
      xfct3p=0.d0
      xfct3l=0.d0
      xfct4=xmatel_four_part(s_red,ypp,jproc0,jinst0)
      xfct5=0.d0
c
      if(scheme.eq.'DI')then
        xfct3p=xkplus(x_ap,abs(icode),lpol)
        xfct3l=xklog(x_ap,abs(icode),lpol)
        if(xi.ne.0.d0)then
          xfct5=xkreg(x_ap,abs(icode),lpol)
        else
          xfct5=xkdelta(abs(icode),lpol)
     #         +xkplus(one,abs(icode),lpol)*log(xicut)
     #         +xklog(one,abs(icode),lpol)*log(xicut)**2/2.d0
c This part contributes to sig2pr(soft), which is integrated in xi
c over the range (0,xicut). This implies the presence of a jacobian
c equal to xicut in the soft term, which has to be removed by hand
c in this case
          xfct5=xfct5/xicut
        endif
      elseif(scheme.ne.'MS')then
        write(6,*)'Error in xmatel_coll, y=',y
        write(6,*)'Factorization scheme ',scheme,' not known'
      endif
c
      xmtel=( xfct1*(xlgsdi_mu+2*log(xi_ev))-xfct2
     #       -xfct3p-xfct3l*log(xi_ev) )*xfct4
      xmtel_sc=-xfct5*xfct4
      return
      end


      function x_sgn(n,m)
c This function returns -1 if N or M is equal to 1 or 2, and 1 otherwise
      implicit real*8 (a-h,o-z)
c
      tmp=1.d0
      if(n.eq.1.or.n.eq.2)tmp=-tmp
      if(m.eq.1.or.m.eq.2)tmp=-tmp
      x_sgn=tmp
      return
      end


      function x_average(jinst)
c This function returns the quantity SIGN*OMEGA(A_1)*OMEGA(A_2), where
c the flavours A_1 and A_2 are equivalently given by JINST, using
c our classification for initial states. The quantity SIGN is -1 if JINST=2
c (<==> qg initial state), 1 otherwise, to take into account the sign coming
c from the crossing of a single fermionic line
      implicit real*8 (a-h,o-z)
      parameter (xnc=3.d0)
      parameter (vda=8.d0)
c
      xsgn=1.d0
      if(jinst.eq.1)then
        tmp=(2*vda)**2
      elseif(jinst.eq.2)then
        tmp=(2*vda)*(2*xnc)
        xsgn=-1.d0
      else
        tmp=(2*xnc)**2
      endif
      x_average=xsgn*tmp
      return
      end


      function eik_mni(xpp,xinv,yi,yj,n,m,ni,nj)
c This function evaluates the eikonal factors 
c         xp_n.xp_m/(xp_n.xp_i xp_m.xp_i)
c times the factor
c         xii**2 (1-yi**2)
c when we evaluate the limiting behaviour of eq.(4.37), or the factor
c         xii**2 (1-yj)
c when we evaluate the limiting behaviour of eq.(4.65). In the former case
c the function is called by fixing yj=2.d0; in the latter case,
c by fixing yi=2.d0
      implicit real*8 (a-h,o-z)
      dimension xpp(1:5,1:5),xinv(1:5)
c
      s=2*xpp(1,2)
      if(yj.eq.2.d0)then
        if(n.eq.1)then
          if(m.eq.2)then
            tmp=8.d0/s
          else
            tmp=8*x_sgn(n,m)*xpp(n,m)*(1+yi)/(s*sqrt(s)*xinv(m))
          endif
        elseif(n.eq.2)then
          tmp=8*x_sgn(n,m)*xpp(n,m)*(1-yi)/(s*sqrt(s)*xinv(m))
        else
          tmp=4*x_sgn(n,m)*xpp(n,m)*(1-yi**2)/(s*xinv(n)*xinv(m))
        endif
      elseif(yi.eq.2.d0)then
        xnum=x_sgn(n,m)*xpp(n,m)
        if(n.eq.nj.or.m.eq.nj)then
          tmp=8*xnum/(s*xinv(m)*xinv(n))
        else
          tmp=4*xnum*(1-yj)/(s*xinv(m)*xinv(n))
        endif
      else
        write(6,*)'Error in eik_mni: wrong yi or yj value'
        stop
      endif
      eik_mni=tmp
      return
      end


      function ap_kern(x,index,lpol)
c This function returns the quantity (1-x)*P_{ab}(x), where
c P_{ab} are the Altarelli-Parisi kernels, and the splitting partons
c {ab} are defined with the following conventions
c
c         index          ab
c
c           1            gg
c           2            qg
c           3            gq
c           4            qq
c
c If lpol=1, the value returned is (1-x)*Delta P_{ab}(x)
      implicit real * 8 (a-h,o-z)
      parameter (vcf=4.d0/3.d0)
      parameter (vtf=1.d0/2.d0)
      parameter (vca=3.d0)
c
      if(lpol.eq.0)then
        if(index.eq.1)then
          ap_kern=2*vca*(x+(1-x)**2/x+x*(1-x)**2)
        elseif(index.eq.2)then
          ap_kern=vtf*(1-x)*(x**2+(1-x)**2)
        elseif(index.eq.3)then
          ap_kern=vcf*(1-x)*(1+(1-x)**2)/x
        elseif(index.eq.4)then
          ap_kern=vcf*(1+x**2)
        else
          write(6,*)'Error in ap_kern: wrong index value'
          stop
        endif
      elseif(lpol.eq.1)then
        if(index.eq.1)then
          ap_kern=vca*((1+x**4)*((1-x)/x+1)-(1-x)**4/x)
        elseif(index.eq.2)then
          ap_kern=vtf*(1-x)*(x**2-(1-x)**2)
        elseif(index.eq.3)then
          ap_kern=vcf*(1-x)*(1-(1-x)**2)/x
        elseif(index.eq.4)then
          ap_kern=vcf*(1+x**2)
        else
          write(6,*)'Error in ap_kern: wrong index value'
          stop
        endif
      else
        write(6,*)'Error in ap_kern: wrong lpol value'
        stop
      endif
      return
      end


      function apprime_kern(x,index,lpol)
c This function returns the quantity (1-x)*P_{ab}^{prime}(x), where
c P_{ab}^{prime} is the ep-dependent part of the Altarelli-Parisi kernels, 
c and the codes for the splitting partons {ab} are defined above.
c If lpol=1, the value returned is (1-x)*Delta P_{ab}^{prime}(x).
c 't Hooft-Veltman definition for gamma_5 has been used
      implicit real * 8 (a-h,o-z)
      parameter (vcf=4.d0/3.d0)
      parameter (vtf=1.d0/2.d0)
      parameter (vca=3.d0)
c
      if(lpol.eq.0)then
        if(index.eq.1)then
          apprime_kern=0.d0
        elseif(index.eq.2)then
          apprime_kern=-2*vtf*x*(1-x)**2
        elseif(index.eq.3)then
          apprime_kern=-vcf*(1-x)*x
        elseif(index.eq.4)then
          apprime_kern=-vcf*(1-x)**2
        else
          write(6,*)'Error in apprime_kern: wrong index value'
          stop
        endif
      elseif(lpol.eq.1)then
        if(index.eq.1)then
          apprime_kern=vca*4*(1-x)**2
        elseif(index.eq.2)then
          apprime_kern=-2*vtf*(1-x)**2
        elseif(index.eq.3)then
          apprime_kern=2*vcf*(1-x)**2
        elseif(index.eq.4)then
          apprime_kern=-vcf*(1-x)**2
        else
          write(6,*)'Error in apprime_kern: wrong index value'
          stop
        endif
      else
        write(6,*)'Error in apprime_kern: wrong lpol value'
        stop
      endif
      return
      end


      function xkdelta(index,lpol)
c This function returns the quantity K^{(d)}_{ab}, relevant for
c the MS --> DIS change in the factorization scheme. 
c The codes for the splitting partons {ab} are defined above
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      parameter (vcf=4.d0/3.d0)
      parameter (vtf=1.d0/2.d0)
      parameter (vca=3.d0)
      parameter (xnc=3.d0)
      common/nl/nl
c
      if(lpol.eq.0)then
        if(index.eq.1)then
          xkdelta=0.d0
        elseif(index.eq.2)then
          xkdelta=0.d0
        elseif(index.eq.3)then
          xkdelta=vcf*(9.d0/2.d0+pi**2/3.d0)
        elseif(index.eq.4)then
          xkdelta=-vcf*(9.d0/2.d0+pi**2/3.d0)
        else
          write(6,*)'Error in xkdelta: wrong index value'
          stop
        endif
      elseif(lpol.eq.1)then
        xkdelta=0.d0
      else
        write(6,*)'Error in xkdelta: wrong lpol value'
        stop
      endif
      return
      end


      function xkplus(x,index,lpol)
c This function returns the quantity K^{(+)}_{ab}(x), relevant for
c the MS --> DIS change in the factorization scheme. Notice that
c there's NO multiplicative (1-x) factor like in the previous functions.
c The codes for the splitting partons {ab} are defined above
      implicit real * 8 (a-h,o-z)
      parameter (vcf=4.d0/3.d0)
      parameter (vtf=1.d0/2.d0)
      parameter (vca=3.d0)
      parameter (xnc=3.d0)
      common/nl/nl
c
      if(lpol.eq.0)then
        if(index.eq.1)then
          xkplus=0.d0
        elseif(index.eq.2)then
          xkplus=0.d0
        elseif(index.eq.3)then
          xkplus=-vcf*(-3.d0/2.d0-(1+x**2)*log(x)+(1-x)*(3+2*x))
        elseif(index.eq.4)then
          xkplus=vcf*(-3.d0/2.d0-(1+x**2)*log(x)+(1-x)*(3+2*x))
        else
          write(6,*)'Error in xkplus: wrong index value'
          stop
        endif
      elseif(lpol.eq.1)then
        xkplus=0.d0
      else
        write(6,*)'Error in xkplus: wrong lpol value'
        stop
      endif
      return
      end


      function xklog(x,index,lpol)
c This function returns the quantity K^{(l)}_{ab}(x), relevant for
c the MS --> DIS change in the factorization scheme. Notice that
c there's NO multiplicative (1-x) factor like in the previous functions.
c The codes for the splitting partons {ab} are defined above
      implicit real * 8 (a-h,o-z)
      parameter (vcf=4.d0/3.d0)
      parameter (vtf=1.d0/2.d0)
      parameter (vca=3.d0)
      parameter (xnc=3.d0)
      common/nl/nl
c
      if(lpol.eq.0)then
        if(index.eq.1)then
          xklog=0.d0
        elseif(index.eq.2)then
          xklog=0.d0
        elseif(index.eq.3)then
          xklog=-vcf*(1+x**2)
        elseif(index.eq.4)then
          xklog=vcf*(1+x**2)
        else
          write(6,*)'Error in xklog: wrong index value'
          stop
        endif
      elseif(lpol.eq.1)then
        xklog=0.d0
      else
        write(6,*)'Error in xklog: wrong lpol value'
        stop
      endif
      return
      end


      function xkreg(x,index,lpol)
c This function returns the quantity K^{(reg)}_{ab}(x), relevant for
c the MS --> DIS change in the factorization scheme. Notice that
c there's NO multiplicative (1-x) factor like in the previous functions.
c The codes for the splitting partons {ab} are defined above
      implicit real * 8 (a-h,o-z)
      parameter (vcf=4.d0/3.d0)
      parameter (vtf=1.d0/2.d0)
      parameter (vca=3.d0)
      parameter (xnc=3.d0)
      common/nl/nl
c
      if(lpol.eq.0)then
        if(index.eq.1)then
          xkreg=-2*nl*vtf*( (x**2+(1-x)**2)*log((1-x)/x)+8*x*(1-x)-1 )
        elseif(index.eq.2)then
          xkreg=vtf*( (x**2+(1-x)**2)*log((1-x)/x)+8*x*(1-x)-1 )
        elseif(index.eq.3)then
          xkreg=0.d0
        elseif(index.eq.4)then
          xkreg=0.d0
        else
          write(6,*)'Error in xkreg: wrong index value'
          stop
        endif
      elseif(lpol.eq.1)then
        xkreg=0.d0
      else
        write(6,*)'Error in xkreg: wrong lpol value'
        stop
      endif
      return
      end


      subroutine xpp_to_ypp(xpp,ypp,jproc,jinst)
c This subroutine fills the array ypp, in such a way that a 2 --> 3
c crossing invariant matrix element function called with entries ypp
c is eventually returning the value of the matrix element for the
c physical process defined by JPROC, JINST
      implicit real * 8 (a-h,o-z)
      common/i_phys/i_phys
      dimension xpp(1:5,1:5),ypp(1:5,1:5)
      integer i_phys(1:5,1:4,1:6)
c
      do i=1,5
        do j=1,5
          ypp(i,j)=xpp(i_phys(i,jproc,jinst),i_phys(j,jproc,jinst))
        enddo
      enddo
      return
      end


      subroutine xpp_to_ypp_0(xpp,ypp,jproc,jinst)
c This subroutine has the same meaning of XPP_TO_YPP, but is relevant
c for the 2 --> 2 processes
      implicit real * 8 (a-h,o-z)
      common/i0_phys/i0_phys
      dimension xpp(1:5,1:5),ypp(1:4,1:4)
      integer i0_phys(1:4,1:4,1:6)
c
      do i=1,4
        do j=1,4
          ypp(i,j)=xpp(i0_phys(i,jproc,jinst),i0_phys(j,jproc,jinst))
        enddo
      enddo
      return
      end


      subroutine x_xp_soft_yp(xpp,ypp,ni)
c This subroutine builds a 2 --> 2 kinematical configuration (ypp)
c starting from a 2 --> 3 kinematical configuration (xpp) in which the
c parton number ni is nearly soft. The relations
c
c      (12)+(13)+(14)=0,   (12)=(34),   (13)=(24),   (14)=(23)
c
c among the 2 --> 2 invariants are exploited
      implicit real * 8 (a-h,o-z)
      common/imtt/imtt
      dimension xpp(1:5,1:5),ypp(1:5,1:5)
      integer imtt(3:4,3:5)
c
      n3=imtt(3,ni)
      ypp(1,2)=xpp(1,2)
      ypp(1,3)=xpp(1,n3)
      ypp(1,4)=-ypp(1,2)-ypp(1,3)
      ypp(2,3)=ypp(1,4)
      ypp(2,4)=ypp(1,3)
      ypp(3,4)=ypp(1,2)
      do i=1,3
        do j=i+1,4
          ypp(j,i)=ypp(i,j)
        enddo
      enddo
      return
      end


      subroutine x_xp_collin_yp(s,xpp,ypp,ni,icode,iflag)
c This subroutine builds a 2 --> 2 kinematical configuration (ypp)
c starting from a 2 --> 3 kinematical configuration (xpp) which is
c nearly collinear (initial state). We use the following conventions
c
c  iflag                     icode
c
c    1   ==>   ni||1           >0   ==>   no action   
c   -1   ==>   ni||2           <0   ==>   ypp(1,*) <--> ypp(2,*)
c
      implicit real * 8 (a-h,o-z)
      common/imtt/imtt
      dimension xpp(1:5,1:5),ypp(1:5,1:5)
      integer imtt(3:4,3:5)
c
      n3=imtt(3,ni)
      ypp(1,2)=s/2
      if(iflag.eq.1)then
        if(icode.gt.0)then
          ypp(2,3)=xpp(2,n3)
          ypp(2,4)=-ypp(1,2)-ypp(2,3)
          ypp(1,3)=ypp(2,4)
          ypp(1,4)=ypp(2,3)
        else
          ypp(1,3)=xpp(2,n3)
          ypp(1,4)=-ypp(1,2)-ypp(1,3)
          ypp(2,3)=ypp(1,4)
          ypp(2,4)=ypp(1,3)
        endif
      elseif(iflag.eq.-1)then
        if(icode.gt.0)then
          ypp(1,3)=xpp(1,n3)
          ypp(1,4)=-ypp(1,2)-ypp(1,3)
          ypp(2,3)=ypp(1,4)
          ypp(2,4)=ypp(1,3)
        else
          ypp(2,3)=xpp(1,n3)
          ypp(2,4)=-ypp(1,2)-ypp(2,3)
          ypp(1,3)=ypp(2,4)
          ypp(1,4)=ypp(2,3)
        endif
      endif
      ypp(3,4)=ypp(1,2)
      do i=1,3
        do j=i+1,4
          ypp(j,i)=ypp(i,j)
        enddo
      enddo
      return
      end


      subroutine x_xp_collout_yp(xpp,ypp,ni,nj,icode)
c This subroutine builds a 2 --> 2 kinematical configuration (ypp)
c starting from a 2 --> 3 kinematical configuration (xpp) which is
c nearly collinear (final state). We use the following conventions 
c (remember that here ni<nj)
c
c   icode    splitting        action on momenta
c
c     1      g --> gg       yk(4,*)=xk(ni,*)+xk(nj,*)
c     2      g --> qqbar    yk(4,*)=xk(ni,*)+xk(nj,*)
c     3      q --> gq       yk(ni,*)=xk(ni,*)+xk(nj,*)
c
      implicit real * 8 (a-h,o-z)
      common/imtt2/imtt2
      dimension xpp(1:5,1:5),ypp(1:5,1:5)
      integer imtt2(3:5,3:5)
c
      n3=imtt2(ni,nj)
      ypp(1,2)=xpp(1,2)
      if(icode.eq.1.or.icode.eq.2)then
        ypp(1,3)=xpp(1,n3)
        ypp(1,4)=-ypp(1,2)-ypp(1,3)
      elseif(icode.eq.3)then
        nn=4*(4-ni)+3*(ni-3)
        ypp(1,nn)=xpp(1,n3)
        ypp(1,ni)=-ypp(1,2)-ypp(1,nn)
      else
        write(6,*)'Error in x_xp_collout_yp: wrong icode'
        stop
      endif
      ypp(2,3)=ypp(1,4)
      ypp(2,4)=ypp(1,3)
      ypp(3,4)=ypp(1,2)
      do i=1,3
        do j=i+1,4
          ypp(j,i)=ypp(i,j)
        enddo
      enddo
      return
      end
c
c Formulae for the cross sections
c
      function p5_ggggg(xpp)
c This function returns the 0 --> 5g matrix element. The spin and color
c average factors are NOT included
      implicit real * 8 (a-z)
      integer lpol
      common/polarized/lpol
      dimension xpp(1:5,1:5)
c
      xpol = dfloat(1-2*lpol)
      x12 = xpp(1,2)
      x13 = xpp(1,3)
      x14 = xpp(1,4)
      x15 = xpp(1,5)
      x23 = xpp(2,3)
      x24 = xpp(2,4)
      x25 = xpp(2,5)
      x34 = xpp(3,4)
      x35 = xpp(3,5)
      x45 = xpp(4,5)
      xf1_3 = 1/(x12*x13*x24*x35*x45)+1/(x12*x14*x23*x35*x45)+1/(x12*x13
     1   *x25*x34*x45)+1/(x12*x15*x23*x34*x45)+1/(x13*x14*x23*x25*x45)+1
     2   /(x13*x15*x23*x24*x45)+1/(x12*x14*x25*x34*x35)+1/(x12*x15*x24*x
     3   34*x35)+1/(x13*x14*x24*x25*x35)+1/(x14*x15*x23*x24*x35)+1/(x13*
     4   x15*x24*x25*x34)+1/(x14*x15*x23*x25*x34)
      xf2 = (x25**4+x24**4+x23**4+x15**4+x14**4+x13**4)*xpol+x45**4+x35*
     1   *4+x34**4+x12**4
      p5_ggggg = 432*xf1_3*xf2
      return 
      end 


      function p5_qqbggg(xpp,jinst)
c This function returns the 0 --> 3g2q matrix element. The spin and color
c average factors are NOT included. The particle labeling is as follows
c
c         0 --> g(1) g(2) g(3) q(4) qb(5)
c
      implicit real*8 (a-z)
      integer lpol,jinst
      common/polarized/lpol
      dimension xpp(1:5,1:5)
c
      xpol = dfloat(1-2*lpol)
      a1=xpp(4,1)                                            
      a2=xpp(4,2)                                            
      a3=xpp(4,3)                                            
      b1=xpp(5,1)                                            
      b2=xpp(5,2)                                            
      b3=xpp(5,3)                                            
      x12=xpp(1,2)                                           
      x23=xpp(2,3)                                           
      x31=xpp(3,1)                                           
      s=2.0*xpp(4,5)                                         
      f1=8.0/(4.0*81.0)                                           
      if(jinst.eq.1)then
        f2_3=(b3**2+a3**2)/(a1*a2*b1*b2)
     #      +xpol*(b2**2+a2**2)/(a1*a3*b1*b3)
     #      +xpol*(b1**2+a1**2)/(a2*a3*b2*b3)
      elseif(jinst.eq.2)then
        f2_3=(xpol*b3**2+a3**2)/(a1*a2*b1*b2)
     #      +(xpol*b2**2+a2**2)/(a1*a3*b1*b3)
     #      +(b1**2+xpol*a1**2)/(a2*a3*b2*b3)
      elseif(jinst.eq.3)then
        f2_3=(b3**2+a3**2)/(a1*a2*b1*b2)
     #      +(b2**2+a2**2)/(a1*a3*b1*b3)
     #      +(b1**2+a1**2)/(a2*a3*b2*b3)
        f2_3=f2_3*xpol
      else
        write(6,*)'Error in p5_qqbggg: wrong jinst value'
        stop
      endif
      f4=(a1*b2+a2*b1)/x12+(a2*b3+a3*b2)/x23+(a3*b1+a1*b3)/x31    
      f4=9.0*(0.5*s-f4)                                           
      f5=a3*b3*(a1*b2+a2*b1)/(x23*x31)+                           
     $   a1*b1*(a2*b3+a3*b2)/(x31*x12)+                           
     $   a2*b2*(a3*b1+a1*b3)/(x12*x23)                            
      f5=f5*2.0*81.0/s                                            
      sum=f1*f2_3*(0.5*s+f4+f5)                                  
      p5_qqbggg=sum*4.0*9.0
      return                                                      
      end                                                         


      function p5_qpqpg(xpp,jinst)
c This function returns the 0 --> 1g2q2Q matrix element. The spin and color
c average factors are NOT included. The particle labeling is as follows
c
c         0 --> Q(1) Qb(2) g(3) q(4) qb(5)
c
      implicit real * 8 (a-z)
      integer lpol,jinst
      parameter (xnc=3.d0)
      common/polarized/lpol
      dimension xpp(1:5,1:5)
c
      xpol = dfloat(1-2*lpol)
      s = 2*xpp(1,4)
      sp = 2*xpp(2,5)
      t = 2*xpp(1,2)
      tp = 2*xpp(4,5)
      u = 2*xpp(1,5)
      up = 2*xpp(2,4)
      x13 = xpp(1,3)
      x34 = xpp(3,4)
      x23 = xpp(2,3)
      x35 = xpp(3,5)
      if(jinst.eq.2)then
        xf1 = (xpol*up**2+u**2+xpol*sp**2+s**2)/(t*tp)
      elseif(jinst.eq.3)then
        xf1 = (up**2+u**2+sp**2+s**2)/(t*tp)
        xf1 = xpol*xf1
      elseif(jinst.eq.5)then
        xf1 = (up**2+u**2+xpol*sp**2+xpol*s**2)/(t*tp)
      elseif(jinst.eq.6)then
        xf1 = (xpol*up**2+xpol*u**2+sp**2+s**2)/(t*tp)
      else
        write(6,*)'Error in p5_qpqpg: wrong jinst value'
        stop
      endif
      xf2_3 = 2*(9*u+t)*x35/(x13*x23*x34)-2*t/(x34*x35)+2*(sp+s)/(x23*x3
     1   5)+14*u/(x13*x35)+2*(8*up+8*u+t)/(x23*x34)+2*(7*u-t)/(x13*x34)+
     2   18*u/(x13*x23)
      p5_qpqpg = (2*xnc)**2*xf1*xf2_3/54.d0
      return 
      end 


      function p5_qqqqg(xpp,jinst)
c This function returns the 0 --> 1g4q matrix element. The spin and color
c average factors are NOT included. The particle labeling is as follows
c
c         0 --> q(1) qb(2) g(3) q(4) qb(5)
c
      implicit real * 8 (a-z)
      integer lpol,jinst
      common/polarized/lpol
      parameter (xnc=3.d0)
      parameter (c1=64.d0/108.d0)
      parameter (c2=c1/8.d0)
      parameter (c3=10.d0/81.d0)
      parameter (c4=8.d0/81.d0)
      dimension xpp(1:5,1:5)
c
      xpol = dfloat(1-2*lpol)
      s = 2*xpp(1,4)
      sp = 2*xpp(2,5)
      t = 2*xpp(1,2)
      tp = 2*xpp(4,5)
      u = 2*xpp(1,5)
      up = 2*xpp(2,4)
      x13 = xpp(1,3)
      x34 = xpp(3,4)
      x23 = xpp(2,3)
      x35 = xpp(3,5)
      if(jinst.eq.2)then
        xg12 = (xpol*up**2+u**2+xpol*sp**2+s**2)/(t*tp)
        xg34 = (tp**2+xpol*t**2+xpol*sp**2+s**2)/(u*up)
        xg56 = (xpol*sp**2+s**2)*(-u*up-t*tp+s*sp)/(t*tp*u*up)
      elseif(jinst.eq.3)then
        xg12 = xpol*(up**2+u**2+sp**2+s**2)/(t*tp)
        xg34 = (tp**2+t**2+xpol*sp**2+xpol*s**2)/(u*up)
        xg56 = xpol*(sp**2+s**2)*(-u*up-t*tp+s*sp)/(t*tp*u*up)
      elseif(jinst.eq.4)then
        xg12 = (xpol*up**2+xpol*u**2+sp**2+s**2)/(t*tp)
        xg34 = (xpol*tp**2+xpol*t**2+sp**2+s**2)/(u*up)
        xg56 = (sp**2+s**2)*(-u*up-t*tp+s*sp)/(t*tp*u*up)
      else
        write(6,*)'Error in p5_qqqqg: wrong jinst value'
        stop
      endif
      xf_3 = (xg56*(-c4*(2*up+tp+t+2*sp)/2.d0-c3*(2*sp+2*s)/2.d0)+
     1   c2*(sp+s)*xg34+c2*(sp+s)*xg12)/(x23*x35)+(xg56*(-c4*(up+u+tp+t)
     2   /2.d0-c3*(-2*u-2*t)/2.d0)+(c2*(-u-t)+c1*t)*xg34+(c1*u+c2*
     3   (-u-t))*xg12)/(x13*x34)+(xg56*(c3*u-c4*(up+u)/2.d0)+(c1*t-c2
     4   *u)*xg34+(c1*u-c2*u)*xg12)/(x13*x35)
      xf_3 = (xg56*(-c4*(up-u+tp+t-2*s)/2.d0-c3*u)+(c2*u+c1*t)*xg34+(
     1   c2*u+c1*u)*xg12)/(x13*x23)+(xg56*(c3*t-c4*(tp+t)/2.d0)+(c1*(
     2   tp+t)-c2*t)*xg34-c2*t*xg12)/(x34*x35)-c4*(up-u)*x34*xg56/(x13*x
     3   23*x35*2.d0)+((c4*(s-up)-c3*t)*xg56+c2*t*xg34+(c1*(up+u)+c2*
     4   t)*xg12)/(x23*x34)+x35*((c3*(-u-t)+c4*s)*xg56+c2*(u+t)*xg34+(c2
     5   *(u+t)+c1*u)*xg12)/(x13*x23*x34)+c1*t*x23*xg34/(x13*x34*x35)+xf
     6   _3
      p5_qqqqg = (2*xnc)**2*xf_3/2.d0
      return 
      end 
c
c End of the 2 --> 3 routines
c
c
c Begin of the 2 --> 2 routines
c
      function xmatel_2pv_contr(s,xpp,jproc,jinst)
c This function is relevant for the evaluation of eq.(5.5).
c It is defined such that
c
c  XMATEL_2PV_CONTR = Q({a_l}) M^{(3,0)}({a_l})
c                   + sum_{m<n} I_{mn}^{reg} M_{mn}^{(3,0)}({a_l})
c                   + M_{NS}^{(3,1)}({a_l})
c
c The sum over final state flavours is performed in the main program
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      common/fixvar/xsc_min2,xlam,xmuf2,xmur2,xmues2,xmuww2,zg,ze2
      common/cutpar/xicut,yincut,youtcut
      common/color_factors/xca,xgmm,xgmmprime
      common/i_type_part/i_type_part
      common/i_type_in/i_type_in
      dimension xpp(1:5,1:5),xca(0:1),xgmm(0:1),xgmmprime(0:1)
      dimension xe(1:4),xpnpm(1:3,2:4)
      integer i_type_part(1:4,1:6,3:5),i_type_in(1:6,1:2)
c
      xlgsd0_qes=log((s*youtcut)/(2*xmues2))
      xlgxi=log(xicut)
      xlgmu_qes=log(xmuf2/xmues2)
c Energy of the partons
      xe(1)=sqrt(s)/2.d0
      xe(2)=sqrt(s)/2.d0
      xe(3)=-(xpp(1,3)+xpp(2,3))/sqrt(s)
      xe(4)=-(xpp(1,4)+xpp(2,4))/sqrt(s)
      do i=1,3
        do j=i+1,4
          xpnpm(i,j)=x_sgn(i,j)*xpp(i,j)
        enddo
      enddo
      xq=0.d0
      do j=3,4
        ip=i_type_part(jproc,jinst,j)
        xq=xq+xgmmprime(ip)
     #    -xlgsd0_qes*( xgmm(ip)
     #                 -2*xca(ip)*log((2*xe(j))/(xicut*sqrt(s))))
     #    +2*xca(ip)*(log((2*xe(j))/sqrt(s))**2-xlgxi**2)
     #    -2*xgmm(ip)*log((2*xe(j))/sqrt(s))
      enddo
      i1=i_type_in(jinst,1)
      i2=i_type_in(jinst,2)
      xq=xq-xlgmu_qes*( xgmm(i1)+2*xca(i1)*xlgxi
     #                + xgmm(i2)+2*xca(i2)*xlgxi )
      xtmp1=xq*xmatel_four_part(s,xpp,jproc,jinst)
c
      xtmp2=0.d0
      do m=1,3
        do n=m+1,4
c In the case when partons n and m are back to back, xlg_4_mn is
c equal to zero, being xlg_4_mn=lim(ep-->0) [ log(2*ep)*log(1-ep/2) ],
c with cos(theta_{mn})=-1+ep ==> treat this case separately
          if((m.eq.1.and.n.eq.2).or.(m.eq.3.and.n.eq.4))then
            xlg_4_mn=0.d0
          else
            xlg_4_mn=log(4.d0-(2*xpnpm(m,n))/(xe(n)*xe(m)))
     #               *log(xpnpm(m,n)/(2*xe(n)*xe(m)))
          endif
          xinm=log(xicut**2*s/xmues2)**2/2.d0
     #        +log(xicut**2*s/xmues2)*log(xpnpm(m,n)/(2*xe(n)*xe(m)))
     #        -ddilog(xpnpm(m,n)/(2*xe(n)*xe(m)))
     #        +log((2*xpnpm(m,n))/(xe(n)*xe(m)))**2/2.d0
     #        -xlg_4_mn-2*log(2.d0)**2
          xtmp2=xtmp2+xinm*x_color_link(s,xpp,jproc,jinst,m,n)/(8*pi**2)
        enddo
      enddo
c
      xmatel_2pv_contr=xtmp1+xtmp2+x_loop_four(s,xpp,jproc,jinst)
      return
      end


      function xmatel_four_part(esq,xpp,jproc,jinst)
c This function has the same meaning of xmatel_five_part, but
c it is relevant for the 2 --> 2 processes. The matrix elements
c are already given in a non crossing invariant form
      implicit real * 8 (a-h,o-z)
      common/polarized/lpol
      dimension xpp(1:5,1:5)
c
      S=2.d0*xpp(1,2)
      T=2.d0*xpp(1,3)
      U=2.d0*xpp(1,4)
c
      if(jproc.eq.1)then
        if(jinst.eq.1)then
          temp=9.d0*(S**2+T**2+U**2)*(S**4+T**4-2.d0*lpol*T**4 
     .        +U**4-2.d0*lpol*U**4)/(8.d0*S**2*T**2*U**2)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.2)then
        if(jinst.eq.1)then
          temp=(1.d0 - 2.d0*lpol)*(4.d0*S**2 - 9.d0*T*U)*
     .         (T**2 + U**2)/(24.d0*S**2*T*U)
        elseif(jinst.eq.2)then
          temp=(4.d0*T**2-9.d0*S*U)*(-S**2-U**2+2.d0*lpol*U**2)/
     .         (9.d0*S*T**2*U)
        elseif(jinst.eq.3)then
          temp=8.d0*(1.d0 - 2.d0*lpol)*(4.d0*S**2 - 9.d0*T*U)*
     .        (T**2 + U**2)/(27.d0*S**2*T*U)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.3)then
        if(jinst.eq.3)then
          temp=4.d0*(1.d0 - 2.d0*lpol)*(T**2 + U**2)/(9.d0*S**2)
        elseif(jinst.eq.5)then
          temp=4.d0*(S**2 + U**2 - 2.d0*lpol*U**2)/(9.d0*T**2)
        elseif(jinst.eq.6)then
          temp=4.d0*(S**2 + U**2 - 2.d0*lpol*U**2)/(9.d0*T**2)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.4)then
        if(jinst.eq.3)then
          temp=4.d0*(3.d0*S**4+3.d0*T**4-6.d0*lpol*T**4-
     .         8.d0*S*T*U**2+16.d0*lpol*S*T*U**2+3.d0*U**4-
     .         6.d0*lpol*U**4)/(27.d0*S**2*T**2)
        elseif(jinst.eq.4)then
          temp=4.d0*(3.d0*S**4+3.d0*T**4-6.d0*lpol*T**4- 
     .         8.d0*S**2*T*U+3.d0*U**4-6.d0*lpol*U**4)/
     .         (27.d0*T**2*U**2)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      else 
        write(6,*)'Error in xmatel_four_part: wrong jproc value'
        stop
      endif
      xmatel_four_part=temp/(2*esq)
      return
      end


      function x_color_link(esq,xpp,jproc,jinst,n1,m1)
c This function gives the color-linked Born squared amplitudes;
c it is relevant for the soft limit of the 2 --> 3 processes. The
c factor 8 pi**2 inserted is consistent with the normalization
c of eq.(3.3)
      implicit real * 8 (a-h,o-z)
      parameter (pi=3.14159265358979312D0)
      common/polarized/lpol
      dimension xpp(1:5,1:5)
c
c Symmetrize over n1 and m1
      m=min(m1,n1)
      n=max(m1,n1)
c
      s=2.d0*xpp(1,2)
      t=2.d0*xpp(1,3)
      u=2.d0*xpp(2,3)
c
      if(jproc.eq.1)then
        if(jinst.eq.1)then
          if( (m.eq.1).and.(n.eq.2)) then
            temp=27*(T**2 + U**2)*(2*S**4 + 2*T**4 - 4*lpol*T**4 +
     .           2*U**4 - 4*lpol*U**4)/(16*S**2*T**2*U**2)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=27*(S**2 + U**2)*(2*S**4 + 2*T**4 - 4*lpol*T**4 +
     .           2*U**4 - 4*lpol*U**4)/(16*S**2*T**2*U**2)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=27*(S**2 + T**2)*(2*S**4 + 2*T**4 - 4*lpol*T**4 +
     .           2*U**4 - 4*lpol*U**4)/(16*S**2*T**2*U**2)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=27*(S**2 + T**2)*(2*S**4 + 2*T**4 - 4*lpol*T**4 +
     .           2*U**4 - 4*lpol*U**4)/(16*S**2*T**2*U**2)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=27*(S**2 + U**2)*(2*S**4 + 2*T**4 - 4*lpol*T**4 + 
     .           2*U**4 - 4*lpol*U**4)/(16*S**2*T**2*U**2)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=27*(T**2 + U**2)*(2*S**4 + 2*T**4 - 4*lpol*T**4 + 
     .           2*U**4 - 4*lpol*U**4)/(16*S**2*T**2*U**2)
          endif
        else
          write(6,*)'Error in x_color_link: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.2)then
        if(jinst.eq.1)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=9*(1 - 2*lpol)*(T**2 + U**2)**2/(16*S**2*T*U)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=(-1+ 2*lpol)*(S - 3*U)*(S + 3*U)*(T**2+U**2)/
     .           (16*S**2*T*U)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=(-1+ 2*lpol)*(S - 3*T)*(S + 3*T)*(T**2+U**2)/
     .           (16*S**2*T*U)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=(-1+ 2*lpol)*(S - 3*T)*(S + 3*T)*(T**2+U**2)/
     .           (16*S**2*T*U)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=(-1+ 2*lpol)*(S - 3*U)*(S + 3*U)*(T**2+U**2)/
     .           (16*S**2*T*U)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=(1 - 2*lpol)*(10*S**2 - 9*T**2 - 9*U**2)*
     .           (T**2 + U**2)/(144*S**2*T*U)
          endif
        elseif(jinst.eq.2)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=(T-3*U)*(T+3*U)*(S**2+U**2-2*lpol*U**2)/(6*S*T**2*U)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=(-9*S**2 + 10*T**2 - 9*U**2)*(-S**2 - U**2 +
     .            2*lpol*U**2)/(54*S*T**2*U)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=(-3*S+T)*(3*S+T)*(S**2+U**2-2*lpol*U**2)/
     .           (6*S*T**2*U)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=(-3*S+T)*(3*S+T)*(S**2+U**2-2*lpol*U**2)/(6*S*T**2*U)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=3*(S**2+U**2)*(-S**2-U**2+2*lpol*U**2)/(2*S*T**2*U)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=(T-3*U)*(T+3*U)*(S**2+U**2-2*lpol*U**2)/(6*S*T**2*U)
          endif
        elseif(jinst.eq.3)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=4*(1 - 2*lpol)*(10*S**2 - 9*T**2 - 9*U**2)*
     .           (T**2 + U**2)/(81*S**2*T*U)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=4*(-1+2*lpol)*(S-3*U)*(S+3*U)*(T**2+U**2)/
     .           (9*S**2*T*U)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=4*(-1+2*lpol)*(S-3*T)*(S+3*T)*(T**2+U**2)/
     .           (9*S**2*T*U)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=4*(-1+2*lpol)*(S-3*T)*(S+3*T)*(T**2+U**2)/
     .           (9*S**2*T*U)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=4*(-1+2*lpol)*(S-3*U)*(S+3*U)*(T**2+U**2)/
     .           (9*S**2*T*U)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=4*(1-2*lpol)*(T**2 + U**2)**2/(S**2*T*U)
          endif
        else
          write(6,*)'Error in x_color_link: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.3)then
        if(jinst.eq.2)then
          temp=0.d0
        elseif(jinst.eq.3)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=4*(-1 + 2*lpol)*(T**2 + U**2)/(27*S**2)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=28*(1 - 2*lpol)*(T**2 + U**2)/(27*S**2)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=8*(1 - 2*lpol)*(T**2 + U**2)/(27*S**2)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=8*(1 - 2*lpol)*(T**2 + U**2)/(27*S**2)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=28*(1 - 2*lpol)*(T**2 + U**2)/(27*S**2)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=4*(-1 + 2*lpol)*(T**2 + U**2)/(27*S**2)
          endif
        elseif(jinst.eq.5)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=28*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=4*(-S**2 - U**2 + 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=8*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=8*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=4*(-S**2 - U**2 + 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=28*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          endif
        elseif(jinst.eq.6)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=8*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=4*(-S**2 - U**2 + 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=28*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=28*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=4*(-S**2 - U**2 + 2*lpol*U**2)/(27*T**2)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=8*(S**2 + U**2 - 2*lpol*U**2)/(27*T**2)
          endif
        else
          write(6,*)'Error in x_color_link: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.4)then
        if(jinst.eq.2)then
          temp=0.d0
        elseif(jinst.eq.3)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=4*(21*S**4-3*T**4+6*lpol*T**4+20*S**2*U**2-
     .           40*lpol*S**2*U**2-4*T**2*U**2+8*lpol*T**2*U**2+U**4-
     .           2*lpol*U**4)/(81*S**2*T**2)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=4*(-3*S**4 + 21*T**4 - 42*lpol*T**4 - 4*S**2*U**2 + 
     .           8*lpol*S**2*U**2 + 20*T**2*U**2 - 40*lpol*T**2*U**2+ 
     .           U**4-2*lpol*U**4)/(81*S**2*T**2)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=8*(3*S**4 + 3*T**4 - 6*lpol*T**4 + 8*S**2*U**2 - 
     .           16*lpol*S**2*U**2+8*T**2*U**2-16*lpol*T**2*U**2-
     .           5*U**4+10*lpol*U**4)/(81*S**2*T**2)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=8*(3*S**4 + 3*T**4 - 6*lpol*T**4 + 8*S**2*U**2 - 
     .           16*lpol*S**2*U**2+8*T**2*U**2-16*lpol*T**2*U**2-
     .           5*U**4+10*lpol*U**4)/(81*S**2*T**2)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=4*(-3*S**4+21*T**4-42*lpol*T**4-4*S**2*U**2 + 
     .           8*lpol*S**2*U**2 + 20*T**2*U**2-40*lpol*T**2*U**2+
     .           U**4-2*lpol*U**4)/(81*S**2*T**2)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=4*(21*S**4 - 3*T**4 + 6*lpol*T**4 + 20*S**2*U**2 - 
     .           40*lpol*S**2*U**2-4*T**2*U**2+8*lpol*T**2*U**2+U**4- 
     .           2*lpol*U**4)/(81*S**2*T**2)
          endif
        elseif(jinst.eq.4)then
          if( (m.eq.1).and.(n.eq.2) ) then
            temp=8*(-5*S**4+8*S**2*T**2+3*T**4-6*lpol*T**4+
     .           8*S**2*U**2+3*U**4-6*lpol*U**4)/(81*T**2*U**2)
          elseif( (m.eq.1).and.(n.eq.3) ) then
            temp=4*(S**4+20*S**2*T**2+21*T**4-42*lpol*T**4-4*S**2*U**2
     .          -3*U**4 + 6*lpol*U**4)/(81*T**2*U**2)
          elseif( (m.eq.1).and.(n.eq.4) ) then
            temp=4*(S**4 -4*S**2*T**2-3*T**4+6*lpol*T**4+20*S**2*U**2 
     .          +21*U**4 - 42*lpol*U**4)/(81*T**2*U**2)
          elseif( (m.eq.2).and.(n.eq.3) ) then
            temp=4*(S**4-4*S**2*T**2-3*T**4+6*lpol*T**4+20*S**2*U**2
     .          +21*U**4 - 42*lpol*U**4)/(81*T**2*U**2)
          elseif( (m.eq.2).and.(n.eq.4) ) then
            temp=4*(S**4 + 20*S**2*T**2 + 21*T**4 - 42*lpol*T**4 - 
     .           4*S**2*U**2 - 3*U**4 + 6*lpol*U**4)/(81*T**2*U**2)
          elseif( (m.eq.3).and.(n.eq.4) ) then
            temp=8*(-5*S**4+8*S**2*T**2+3*T**4-6*lpol*T**4+
     .           8*S**2*U**2+3*U**4-6*lpol*U**4)/(81*T**2*U**2)
          endif
        else
          write(6,*)'Error in x_color_link: wrong jinst value'
          stop
        endif
      else 
        write(6,*)'Error in x_color_link: wrong jinst value'
        stop
      endif
      x_color_link=8*pi**2*temp/(2*esq)
      return
      end


      double precision function x_loop_four(esq,xpp,jproc,jinst)
c This function returns the M_{NS}^{(3,1)} part of the virtual
c contribution, as defined in eq. (3.2)
      IMPLICIT NONE
      DOUBLE PRECISION S,T,U,esq,temp 
      DOUBLE PRECISION THETA
      DOUBLE PRECISION LS,LT,LU,LMU,L2S,L2T,L2U
      DOUBLE PRECISION XSC_MIN2,XLAM,XMUF2,XMUR2,XMUES2,XMUWW2,ZG,ZE2
      DOUBLE PRECISION PI,NF,MUUV,QES
      double precision xpp(1:5,1:5)
      INTEGER NL,jproc,jinst,lpol
      COMMON/FIXVAR/XSC_MIN2,XLAM,XMUF2,XMUR2,XMUES2,XMUWW2,ZG,ZE2
      COMMON/NL/NL
      common/polarized/lpol
      PARAMETER ( PI = 3.141592654 D0)
C
      NF=DFLOAT(NL)
      MUUV=DSQRT(XMUF2)
      QES=DSQRT(XMUES2)
C
      S = 2.0D0 * xpp(1,2)
      T = 2.0D0 * xpp(1,3)
      U = 2.0D0 * xpp(1,4)
C
C  Here are the Ellis and Sexton log functions
C
      LS = DLOG(DABS(S/QES**2))
      LT = DLOG(DABS(T/QES**2))
      LU = DLOG(DABS(U/QES**2))
      LMU = DLOG(DABS(MUUV**2/QES**2))
      L2S = LS**2 - PI**2 * THETA(S.GT.0.0D0)
      L2T = LT**2 - PI**2 * THETA(T.GT.0.0D0)
      L2U = LU**2 - PI**2 * THETA(U.GT.0.0D0)
      if(jproc.eq.1)then
        if(jinst.eq.1)then
       temp=-402*S**6+198*LMU*S**6+99*LT*S**6+
     . 99*LU*S**6-108*LT*LU*S**6+
     . 20*NF*S**6-12*LMU*NF*S**6-6*LT*NF*S**6-6*LU*NF*S**6+
     . 54*PI**2*S**6+99*LT*S**5*T-99*LU*S**5*T-6*LT*NF*S**5*T+
     . 6*LU*NF*S**5*T-402*S**4*T**2+198*LMU*S**4*T**2+
     . 198*LU*S**4*T**2-108*LS*LU*S**4*T**2+20*NF*S**4*T**2-
     . 12*LMU*NF*S**4*T**2-12*LU*NF*S**4*T**2+54*PI**2*S**4*T**2-
     . 402*S**2*T**4+198*LMU*S**2*T**4+804*lpol*S**2*T**4-
     . 396*LMU*lpol*S**2*T**4+198*LU*S**2*T**4-396*lpol*LU*S**2*T**4-
     . 108*LT*LU*S**2*T**4+216*lpol*LT*LU*S**2*T**4+20*NF*S**2*T**4-
     . 12*LMU*NF*S**2*T**4-40*lpol*NF*S**2*T**4+
     . 24*LMU*lpol*NF*S**2*T**4-12*LU*NF*S**2*T**4+
     . 24*lpol*LU*NF*S**2*T**4+54*PI**2*S**2*T**4-
     . 108*lpol*PI**2*S**2*T**4+99*LS*S*T**5-198*lpol*LS*S*T**5-
     . 99*LU*S*T**5+198*lpol*LU*S*T**5-6*LS*NF*S*T**5+
     . 12*lpol*LS*NF*S*T**5+6*LU*NF*S*T**5-12*lpol*LU*NF*S*T**5-
     . 402*T**6+198*LMU*T**6+804*lpol*T**6-396*LMU*lpol*T**6+
     . 99*LS*T**6-198*lpol*LS*T**6+99*LU*T**6-198*lpol*LU*T**6-
     . 108*LS*LU*T**6+216*lpol*LS*LU*T**6+20*NF*T**6-12*LMU*NF*T**6-
     . 40*lpol*NF*T**6+24*LMU*lpol*NF*T**6-6*LS*NF*T**6+
     . 12*lpol*LS*NF*T**6-6*LU*NF*T**6+12*lpol*LU*NF*T**6+
     . 54*PI**2*T**6-108*lpol*PI**2*T**6-99*LT*S**5*U+99*LU*S**5*U+
     . 6*LT*NF*S**5*U-6*LU*NF*S**5*U-54*S**4*T*U+216*LT*LU*S**4*T*U-
     . 108*L2T*S**4*T*U-108*L2U*S**4*T*U+18*NF*S**4*T*U-
     . 18*LT*LU*NF*S**4*T*U+9*L2T*NF*S**4*T*U+9*L2U*NF*S**4*T*U-
     . 108*PI**2*S**4*T*U+9*NF*PI**2*S**4*T*U-54*LT*S**3*T**2*U+
     . 54*LU*S**3*T**2*U+18*LT*NF*S**3*T**2*U-18*LU*NF*S**3*T**2*U-
     . 54*LS*S**2*T**3*U+108*lpol*LS*S**2*T**3*U+54*LU*S**2*T**3*U-
     . 108*lpol*LU*S**2*T**3*U+18*LS*NF*S**2*T**3*U-
     . 36*lpol*LS*NF*S**2*T**3*U-18*LU*NF*S**2*T**3*U+
     . 36*lpol*LU*NF*S**2*T**3*U-54*S*T**4*U+108*lpol*S*T**4*U+
     . 216*LS*LU*S*T**4*U-432*lpol*LS*LU*S*T**4*U-108*L2S*S*T**4*U+
     . 216*lpol*L2S*S*T**4*U-108*L2U*S*T**4*U+216*lpol*L2U*S*T**4*U+
     . 18*NF*S*T**4*U-36*lpol*NF*S*T**4*U-18*LS*LU*NF*S*T**4*U+
     . 36*lpol*LS*LU*NF*S*T**4*U+9*L2S*NF*S*T**4*U-
     . 18*lpol*L2S*NF*S*T**4*U+9*L2U*NF*S*T**4*U-
     . 18*lpol*L2U*NF*S*T**4*U-108*PI**2*S*T**4*U+
     . 216*lpol*PI**2*S*T**4*U+9*NF*PI**2*S*T**4*U-
     . 18*lpol*NF*PI**2*S*T**4*U-99*LS*T**5*U+198*lpol*LS*T**5*U+
     . 99*LU*T**5*U-198*lpol*LU*T**5*U+6*LS*NF*T**5*U-
     . 12*lpol*LS*NF*T**5*U-6*LU*NF*T**5*U+12*lpol*LU*NF*T**5*U-
     . 402*S**4*U**2+198*LMU*S**4*U**2+198*LT*S**4*U**2-
     . 108*LS*LT*S**4*U**2+20*NF*S**4*U**2-12*LMU*NF*S**4*U**2-
     . 12*LT*NF*S**4*U**2+54*PI**2*S**4*U**2+54*LT*S**3*T*U**2-
     . 54*LU*S**3*T*U**2-18*LT*NF*S**3*T*U**2+18*LU*NF*S**3*T*U**2-
     . 108*LS*LT*S**2*T**2*U**2+216*lpol*LS*LT*S**2*T**2*U**2-
     . 108*LS*LU*S**2*T**2*U**2+216*lpol*LS*LU*S**2*T**2*U**2-
     . 108*LT*LU*S**2*T**2*U**2+108*L2S*S**2*T**2*U**2-
     . 216*lpol*L2S*S**2*T**2*U**2+108*L2T*S**2*T**2*U**2-
     . 108*lpol*L2T*S**2*T**2*U**2+108*L2U*S**2*T**2*U**2-
     . 108*lpol*L2U*S**2*T**2*U**2+36*LS*LT*NF*S**2*T**2*U**2-
     . 72*lpol*LS*LT*NF*S**2*T**2*U**2+36*LS*LU*NF*S**2*T**2*U**2-
     . 72*lpol*LS*LU*NF*S**2*T**2*U**2+36*LT*LU*NF*S**2*T**2*U**2-
     . 36*L2S*NF*S**2*T**2*U**2+72*lpol*L2S*NF*S**2*T**2*U**2-
     . 36*L2T*NF*S**2*T**2*U**2+36*lpol*L2T*NF*S**2*T**2*U**2-
     . 36*L2U*NF*S**2*T**2*U**2+36*lpol*L2U*NF*S**2*T**2*U**2+
     . 162*PI**2*S**2*T**2*U**2-216*lpol*PI**2*S**2*T**2*U**2-
     . 54*NF*PI**2*S**2*T**2*U**2+72*lpol*NF*PI**2*S**2*T**2*U**2+
     . 54*LS*S*T**3*U**2-108*lpol*LS*S*T**3*U**2-54*LU*S*T**3*U**2+
     . 108*lpol*LU*S*T**3*U**2-18*LS*NF*S*T**3*U**2+
     . 36*lpol*LS*NF*S*T**3*U**2+18*LU*NF*S*T**3*U**2-
     . 36*lpol*LU*NF*S*T**3*U**2-402*T**4*U**2+198*LMU*T**4*U**2+
     . 804*lpol*T**4*U**2-396*LMU*lpol*T**4*U**2+198*LS*T**4*U**2-
     . 396*lpol*LS*T**4*U**2-108*LS*LT*T**4*U**2+
     . 216*lpol*LS*LT*T**4*U**2+20*NF*T**4*U**2-12*LMU*NF*T**4*U**2-
     . 40*lpol*NF*T**4*U**2+24*LMU*lpol*NF*T**4*U**2-
     . 12*LS*NF*T**4*U**2+24*lpol*LS*NF*T**4*U**2+54*PI**2*T**4*U**2-
     . 108*lpol*PI**2*T**4*U**2-54*LS*S**2*T*U**3
	temp=temp+
     . 108*lpol*LS*S**2*T*U**3+54*LT*S**2*T*U**3-
     . 108*lpol*LT*S**2*T*U**3+18*LS*NF*S**2*T*U**3-
     . 36*lpol*LS*NF*S**2*T*U**3-18*LT*NF*S**2*T*U**3+
     . 36*lpol*LT*NF*S**2*T*U**3+54*LS*S*T**2*U**3-
     . 108*lpol*LS*S*T**2*U**3-54*LT*S*T**2*U**3+
     . 108*lpol*LT*S*T**2*U**3-18*LS*NF*S*T**2*U**3+
     . 36*lpol*LS*NF*S*T**2*U**3+18*LT*NF*S*T**2*U**3-
     . 36*lpol*LT*NF*S*T**2*U**3-402*S**2*U**4+198*LMU*S**2*U**4+
     . 804*lpol*S**2*U**4-396*LMU*lpol*S**2*U**4+198*LT*S**2*U**4-
     . 396*lpol*LT*S**2*U**4-108*LT*LU*S**2*U**4+
     . 216*lpol*LT*LU*S**2*U**4+20*NF*S**2*U**4-12*LMU*NF*S**2*U**4-
     . 40*lpol*NF*S**2*U**4+24*LMU*lpol*NF*S**2*U**4-
     . 12*LT*NF*S**2*U**4+24*lpol*LT*NF*S**2*U**4+54*PI**2*S**2*U**4-
     . 108*lpol*PI**2*S**2*U**4-54*S*T*U**4+108*lpol*S*T*U**4+
     . 216*LS*LT*S*T*U**4-432*lpol*LS*LT*S*T*U**4-108*L2S*S*T*U**4+
     . 216*lpol*L2S*S*T*U**4-108*L2T*S*T*U**4+216*lpol*L2T*S*T*U**4+
     . 18*NF*S*T*U**4-36*lpol*NF*S*T*U**4-18*LS*LT*NF*S*T*U**4+
     . 36*lpol*LS*LT*NF*S*T*U**4+9*L2S*NF*S*T*U**4-
     . 18*lpol*L2S*NF*S*T*U**4+9*L2T*NF*S*T*U**4-
     . 18*lpol*L2T*NF*S*T*U**4-108*PI**2*S*T*U**4+
     . 216*lpol*PI**2*S*T*U**4+9*NF*PI**2*S*T*U**4-
     . 18*lpol*NF*PI**2*S*T*U**4-402*T**2*U**4+198*LMU*T**2*U**4+
     . 804*lpol*T**2*U**4-396*LMU*lpol*T**2*U**4+198*LS*T**2*U**4-
     . 396*lpol*LS*T**2*U**4-108*LS*LU*T**2*U**4+
     . 216*lpol*LS*LU*T**2*U**4+20*NF*T**2*U**4-12*LMU*NF*T**2*U**4-
     . 40*lpol*NF*T**2*U**4+24*LMU*lpol*NF*T**2*U**4-
     . 12*LS*NF*T**2*U**4+24*lpol*LS*NF*T**2*U**4+54*PI**2*T**2*U**4-
     . 108*lpol*PI**2*T**2*U**4+99*LS*S*U**5-198*lpol*LS*S*U**5-
     . 99*LT*S*U**5+198*lpol*LT*S*U**5-6*LS*NF*S*U**5+
     . 12*lpol*LS*NF*S*U**5+6*LT*NF*S*U**5-12*lpol*LT*NF*S*U**5-
     . 99*LS*T*U**5+198*lpol*LS*T*U**5+99*LT*T*U**5-
     . 198*lpol*LT*T*U**5+6*LS*NF*T*U**5-12*lpol*LS*NF*T*U**5-
     . 6*LT*NF*T*U**5+12*lpol*LT*NF*T*U**5-402*U**6+198*LMU*U**6+
     . 804*lpol*U**6-396*LMU*lpol*U**6+99*LS*U**6-198*lpol*LS*U**6+
     . 99*LT*U**6-198*lpol*LT*U**6-108*LS*LT*U**6+
     . 216*lpol*LS*LT*U**6+20*NF*U**6-12*LMU*NF*U**6-40*lpol*NF*U**6+
     . 24*LMU*lpol*NF*U**6-6*LS*NF*U**6+12*lpol*LS*NF*U**6-
     . 6*LT*NF*U**6+12*lpol*LT*NF*U**6+54*PI**2*U**6-
     . 108*lpol*PI**2*U**6
	temp=temp/(16.d0*S**2*T**2*U**2)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.2)then
        if(jinst.eq.1)then
          temp= -448*T**4+528*LMU*T**4-40*LS*LT*T**4+
     . 192*LU*T**4-164*LS*LU*T**4+
     . 36*LT*LU*T**4+20*L2S*T**4+20*L2T*T**4-80*L2U*T**4-
     . 32*LMU*NF*T**4+84*PI**2*T**4+22*T**3*U-132*LMU*T**3*U-
     . 76*LS*T**3*U+20*LT*T**3*U+240*LS*LT*T**3*U+8*LU*T**3*U-
     . 26*LS*LU*T**3*U+72*LT*LU*T**3*U-127*L2S*T**3*U-
     . 120*L2T*T**3*U+13*L2U*T**3*U+8*LMU*NF*T**3*U-
     . 143*PI**2*T**3*U-716*T**2*U**2+1056*LMU*T**2*U**2+
     . 712*LS*T**2*U**2-164*LT*T**2*U**2-230*LS*LT*T**2*U**2-
     . 164*LU*T**2*U**2-230*LS*LU*T**2*U**2+72*LT*LU*T**2*U**2+
     . 66*L2S*T**2*U**2-47*L2T*T**2*U**2-47*L2U*T**2*U**2-
     . 64*LMU*NF*T**2*U**2+194*PI**2*T**2*U**2+22*T*U**3-
     . 132*LMU*T*U**3-76*LS*T*U**3+8*LT*T*U**3-26*LS*LT*T*U**3+
     . 20*LU*T*U**3+240*LS*LU*T*U**3+72*LT*LU*T*U**3-
     . 127*L2S*T*U**3+13*L2T*T*U**3-120*L2U*T*U**3+8*LMU*NF*T*U**3-
     . 143*PI**2*T*U**3-448*U**4+528*LMU*U**4+192*LT*U**4-
     . 164*LS*LT*U**4-40*LS*LU*U**4+36*LT*LU*U**4+20*L2S*U**4-
     . 80*L2T*U**4+20*L2U*U**4-32*LMU*NF*U**4+84*PI**2*U**4
	temp=temp*2.d0*(1.d0-2.d0*lpol)/(576.d0*S**2*T*U)
       elseif(jinst.eq.2)then
          temp= 448*T**4-528*LMU*T**4+40*LS*LT*T**4-
     . 40*lpol*LS*LT*T**4-192*LU*T**4-
     . 36*LS*LU*T**4+164*LT*LU*T**4-20*L2S*T**4+20*lpol*L2S*T**4-
     . 20*L2T*T**4+20*lpol*L2T*T**4+80*L2U*T**4+32*LMU*NF*T**4-
     . 84*PI**2*T**4+20*lpol*PI**2*T**4+1814*T**3*U-2244*LMU*T**3*U+
     . 20*LS*T**3*U-40*lpol*LS*T**3*U-76*LT*T**3*U+40*lpol*LT*T**3*U+
     . 400*LS*LT*T**3*U-360*lpol*LS*LT*T**3*U-760*LU*T**3*U-
     . 72*LS*LU*T**3*U+630*LT*LU*T**3*U-200*L2S*T**3*U+
     . 180*lpol*L2S*T**3*U-207*L2T*T**3*U+180*lpol*L2T*T**3*U+
     . 333*L2U*T**3*U+136*LMU*NF*T**3*U-479*PI**2*T**3*U+
     . 180*lpol*PI**2*T**3*U+3470*T**2*U**2-4620*LMU*T**2*U**2-
     . 716*lpol*T**2*U**2+1056*LMU*lpol*T**2*U**2+224*LS*T**2*U**2-
     . 448*lpol*LS*T**2*U**2-940*LT*T**2*U**2+832*lpol*LT*T**2*U**2+
     . 1190*LS*LT*T**2*U**2-972*lpol*LS*LT*T**2*U**2-964*LU*T**2*U**2-
     . 72*LS*LU*T**2*U**2+72*lpol*LS*LU*T**2*U**2+1136*LT*LU*T**2*U**2-
     . 328*lpol*LT*LU*T**2*U**2-433*L2S*T**2*U**2+
     . 486*lpol*L2S*T**2*U**2-567*L2T*T**2*U**2+486*lpol*L2T*T**2*U**2+
     . 566*L2U*T**2*U**2-160*lpol*L2U*T**2*U**2+280*LMU*NF*T**2*U**2-
     . 64*LMU*lpol*NF*T**2*U**2-1127*PI**2*T**2*U**2+
     . 614*lpol*PI**2*T**2*U**2+3312*T*U**3-4752*LMU*T*U**3-
     . 1476*lpol*T*U**3+2376*LMU*lpol*T*U**3+396*LS*T*U**3-
     . 792*lpol*LS*T*U**3-1728*LT*T*U**3+1656*lpol*LT*T*U**3+
     . 1314*LS*LT*T*U**3-972*lpol*LS*LT*T*U**3-396*LU*T*U**3+
     . 1278*LT*LU*T*U**3-936*lpol*LT*LU*T*U**3-333*L2S*T*U**3+
     . 486*lpol*L2S*T*U**3-720*L2T*T*U**3+666*lpol*L2T*T*U**3+
     . 333*L2U*T*U**3-180*lpol*L2U*T*U**3+288*LMU*NF*T*U**3-
     . 144*LMU*lpol*NF*T*U**3-1296*PI**2*T*U**3+954*lpol*PI**2*T*U**3+
     . 1656*U**4-2376*LMU*U**4-1656*lpol*U**4+2376*LMU*lpol*U**4-
     . 864*LT*U**4+864*lpol*LT*U**4+648*LS*LT*U**4-
     . 648*lpol*LS*LT*U**4+648*LT*LU*U**4-648*lpol*LT*LU*U**4-
     . 360*L2T*U**4+360*lpol*L2T*U**4+144*LMU*NF*U**4-
     . 144*LMU*lpol*NF*U**4-648*PI**2*U**4+648*lpol*PI**2*U**4
	temp=temp*2.d0/(216.d0*S*T**2*U)
        elseif(jinst.eq.3)then
          temp=-448*T**4+528*LMU*T**4-40*LS*LT*T**4+
     . 192*LU*T**4-164*LS*LU*T**4+
     . 36*LT*LU*T**4+20*L2S*T**4+20*L2T*T**4-80*L2U*T**4-
     . 32*LMU*NF*T**4+84*PI**2*T**4+22*T**3*U-132*LMU*T**3*U-
     . 76*LS*T**3*U+20*LT*T**3*U+240*LS*LT*T**3*U+8*LU*T**3*U-
     . 26*LS*LU*T**3*U+72*LT*LU*T**3*U-127*L2S*T**3*U-120*L2T*T**3*U+
     . 13*L2U*T**3*U+8*LMU*NF*T**3*U-143*PI**2*T**3*U-716*T**2*U**2+
     . 1056*LMU*T**2*U**2+712*LS*T**2*U**2-164*LT*T**2*U**2-
     . 230*LS*LT*T**2*U**2-164*LU*T**2*U**2-230*LS*LU*T**2*U**2+
     . 72*LT*LU*T**2*U**2+66*L2S*T**2*U**2-47*L2T*T**2*U**2-
     . 47*L2U*T**2*U**2-64*LMU*NF*T**2*U**2+194*PI**2*T**2*U**2+
     . 22*T*U**3-132*LMU*T*U**3-76*LS*T*U**3+8*LT*T*U**3-
     . 26*LS*LT*T*U**3+20*LU*T*U**3+240*LS*LU*T*U**3+72*LT*LU*T*U**3-
     . 127*L2S*T*U**3+13*L2T*T*U**3-120*L2U*T*U**3+8*LMU*NF*T*U**3-
     . 143*PI**2*T*U**3-448*U**4+528*LMU*U**4+192*LT*U**4-
     . 164*LS*LT*U**4-40*LS*LU*U**4+36*LT*LU*U**4+20*L2S*U**4-
     . 80*L2T*U**4+20*L2U*U**4-32*LMU*NF*U**4+84*PI**2*U**4
	temp=temp*2.d0*(1.d0-2.d0*lpol)/(81.d0*S**2*T*U)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
       elseif(jproc.eq.3)then
        if(jinst.eq.3)then
          temp=12*LS*S*T+42*LS*LT*S*T-12*LU*S*T-
     . 12*LS*LU*S*T-15*L2S*S*T-
     . 21*L2T*S*T+6*L2U*S*T-15*PI**2*S*T+126*T**2+198*LMU*T**2-
     . 54*LS*T**2-84*LS*LT*T**2-24*LS*LU*T**2+60*L2S*T**2-
     . 20*NF*T**2-12*LMU*NF*T**2+12*LS*NF*T**2+54*PI**2*T**2+
     . 42*LS*S*U-42*LT*S*U-42*LS*LT*S*U+12*LS*LU*S*U+15*L2S*S*U+
     . 21*L2T*S*U-6*L2U*S*U+15*PI**2*S*U+126*U**2+198*LMU*U**2-
     . 54*LS*U**2-84*LS*LT*U**2-24*LS*LU*U**2+60*L2S*U**2-
     . 20*NF*U**2-12*LMU*NF*U**2+12*LS*NF*U**2+54*PI**2*U**2
 	temp=temp*2.d0*(1.d0-2.d0*lpol)/(81.d0*S**2)
        elseif(jinst.eq.5)then
          temp= 126*S**2+198*LMU*S**2-54*LT*S**2-
     . 84*LS*LT*S**2-24*LT*LU*S**2+
     . 60*L2T*S**2-20*NF*S**2-12*LMU*NF*S**2+12*LT*NF*S**2+
     . 54*PI**2*S**2+12*LT*S*T+42*LS*LT*S*T-84*lpol*LS*LT*S*T-
     . 12*LU*S*T-12*LT*LU*S*T-21*L2S*S*T+42*lpol*L2S*S*T-
     . 15*L2T*S*T+42*lpol*L2T*S*T+6*L2U*S*T-15*PI**2*S*T+
     . 42*lpol*PI**2*S*T-42*LS*T*U+84*lpol*LS*T*U+42*LT*T*U-
     . 84*lpol*LT*T*U-42*LS*LT*T*U+84*lpol*LS*LT*T*U+12*LT*LU*T*U+
     . 21*L2S*T*U-42*lpol*L2S*T*U+15*L2T*T*U-42*lpol*L2T*T*U-
     . 6*L2U*T*U+15*PI**2*T*U-42*lpol*PI**2*T*U+126*U**2+
     . 198*LMU*U**2-252*lpol*U**2-396*LMU*lpol*U**2-54*LT*U**2+
     . 108*lpol*LT*U**2-84*LS*LT*U**2+168*lpol*LS*LT*U**2-
     . 24*LT*LU*U**2+48*lpol*LT*LU*U**2+60*L2T*U**2-
     . 120*lpol*L2T*U**2-20*NF*U**2-12*LMU*NF*U**2+40*lpol*NF*U**2+
     . 24*LMU*lpol*NF*U**2+12*LT*NF*U**2-24*lpol*LT*NF*U**2+
     . 54*PI**2*U**2-108*lpol*PI**2*U**2
  	temp=temp*2.d0/(81.d0*T**2)
        elseif(jinst.eq.6)then
          temp= 126*S**2+198*LMU*S**2-54*LT*S**2-
     . 24*LS*LT*S**2-84*LT*LU*S**2+
     . 60*L2T*S**2-20*NF*S**2-12*LMU*NF*S**2+12*LT*NF*S**2+
     . 54*PI**2*S**2+42*LT*S*T+12*LS*LT*S*T-24*lpol*LS*LT*S*T-
     . 42*LU*S*T-42*LT*LU*S*T-6*L2S*S*T+12*lpol*L2S*S*T+15*L2T*S*T+
     . 12*lpol*L2T*S*T+21*L2U*S*T+15*PI**2*S*T+12*lpol*PI**2*S*T-
     . 12*LS*T*U+24*lpol*LS*T*U+12*LT*T*U-24*lpol*LT*T*U-
     . 12*LS*LT*T*U+24*lpol*LS*LT*T*U+42*LT*LU*T*U+6*L2S*T*U-
     . 12*lpol*L2S*T*U-15*L2T*T*U-12*lpol*L2T*T*U-21*L2U*T*U-
     . 15*PI**2*T*U-12*lpol*PI**2*T*U+126*U**2+198*LMU*U**2-
     . 252*lpol*U**2-396*LMU*lpol*U**2-54*LT*U**2+108*lpol*LT*U**2-
     . 24*LS*LT*U**2+48*lpol*LS*LT*U**2-84*LT*LU*U**2+
     . 168*lpol*LT*LU*U**2+60*L2T*U**2-120*lpol*L2T*U**2-20*NF*U**2-
     . 12*LMU*NF*U**2+40*lpol*NF*U**2+24*LMU*lpol*NF*U**2+
     . 12*LT*NF*U**2-24*lpol*LT*NF*U**2+54*PI**2*U**2-
     . 108*lpol*PI**2*U**2
   	temp=temp*2.d0/(81.d0*T**2)
        else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      elseif(jproc.eq.4)then
        if(jinst.eq.3)then
          temp=378*S**4+594*LMU*S**4-162*LT*S**4-
     . 252*LS*LT*S**4-72*LT*LU*S**4+
     . 180*L2T*S**4-60*NF*S**4-36*LMU*NF*S**4+36*LT*NF*S**4+
     . 162*PI**2*S**4+36*LT*S**3*T+126*LS*LT*S**3*T-
     . 252*lpol*LS*LT*S**3*T-36*LU*S**3*T-36*LT*LU*S**3*T-
     . 63*L2S*S**3*T+126*lpol*L2S*S**3*T-45*L2T*S**3*T+
     . 126*lpol*L2T*S**3*T+18*L2U*S**3*T-45*PI**2*S**3*T+
     . 126*lpol*PI**2*S**3*T+12*LS*LT*S**2*T**2-
     . 24*lpol*LS*LT*S**2*T**2-6*L2S*S**2*T**2+12*lpol*L2S*S**2*T**2-
     . 6*L2T*S**2*T**2+12*lpol*L2T*S**2*T**2-6*PI**2*S**2*T**2+
     . 12*lpol*PI**2*S**2*T**2+36*LS*S*T**3-72*lpol*LS*S*T**3+
     . 126*LS*LT*S*T**3-252*lpol*LS*LT*S*T**3-36*LU*S*T**3+
     . 72*lpol*LU*S*T**3-36*LS*LU*S*T**3+72*lpol*LS*LU*S*T**3-
     . 45*L2S*S*T**3+90*lpol*L2S*S*T**3-63*L2T*S*T**3+
     . 126*lpol*L2T*S*T**3+18*L2U*S*T**3-36*lpol*L2U*S*T**3-
     . 45*PI**2*S*T**3+90*lpol*PI**2*S*T**3+378*T**4+594*LMU*T**4-
     . 756*lpol*T**4-1188*LMU*lpol*T**4-162*LS*T**4+324*lpol*LS*T**4-
     . 252*LS*LT*T**4+504*lpol*LS*LT*T**4-72*LS*LU*T**4+
     . 144*lpol*LS*LU*T**4+180*L2S*T**4-360*lpol*L2S*T**4-60*NF*T**4-
     . 36*LMU*NF*T**4+120*lpol*NF*T**4+72*LMU*lpol*NF*T**4+
     . 36*LS*NF*T**4-72*lpol*LS*NF*T**4+162*PI**2*T**4-
     . 324*lpol*PI**2*T**4-120*LS*S**2*T*U+240*lpol*LS*S**2*T*U+
     . 120*LT*S**2*T*U-240*lpol*LT*S**2*T*U-132*LS*LT*S**2*T*U+
     . 264*lpol*LS*LT*S**2*T*U+36*LT*LU*S**2*T*U+66*L2S*S**2*T*U-
     . 132*lpol*L2S*S**2*T*U+48*L2T*S**2*T*U-132*lpol*L2T*S**2*T*U-
     . 18*L2U*S**2*T*U+48*PI**2*S**2*T*U-132*lpol*PI**2*S**2*T*U+
     . 120*LS*S*T**2*U-240*lpol*LS*S*T**2*U-120*LT*S*T**2*U+
     . 240*lpol*LT*S*T**2*U-132*LS*LT*S*T**2*U+264*lpol*LS*LT*S*T**2*U+
     . 36*LS*LU*S*T**2*U-72*lpol*LS*LU*S*T**2*U+48*L2S*S*T**2*U-
     . 96*lpol*L2S*S*T**2*U+66*L2T*S*T**2*U-132*lpol*L2T*S*T**2*U-
     . 18*L2U*S*T**2*U+36*lpol*L2U*S*T**2*U+48*PI**2*S*T**2*U-
     . 96*lpol*PI**2*S*T**2*U+378*S**2*U**2+594*LMU*S**2*U**2-
     . 756*lpol*S**2*U**2-1188*LMU*lpol*S**2*U**2-162*LT*S**2*U**2+
     . 324*lpol*LT*S**2*U**2-252*LS*LT*S**2*U**2+
     . 504*lpol*LS*LT*S**2*U**2-72*LT*LU*S**2*U**2+
     . 144*lpol*LT*LU*S**2*U**2+180*L2T*S**2*U**2-
     . 360*lpol*L2T*S**2*U**2-60*NF*S**2*U**2-36*LMU*NF*S**2*U**2+
     . 120*lpol*NF*S**2*U**2+72*LMU*lpol*NF*S**2*U**2+
     . 36*LT*NF*S**2*U**2-72*lpol*LT*NF*S**2*U**2+162*PI**2*S**2*U**2-
     . 324*lpol*PI**2*S**2*U**2-252*S*T*U**2-396*LMU*S*T*U**2+
     . 504*lpol*S*T*U**2+792*LMU*lpol*S*T*U**2+54*LS*S*T*U**2-
     . 108*lpol*LS*S*T*U**2+54*LT*S*T*U**2-108*lpol*LT*S*T*U**2-
     . 24*LS*LT*S*T*U**2+48*lpol*LS*LT*S*T*U**2+120*LS*LU*S*T*U**2-
     . 240*lpol*LS*LU*S*T*U**2+120*LT*LU*S*T*U**2-
     . 240*lpol*LT*LU*S*T*U**2-60*L2S*S*T*U**2+120*lpol*L2S*S*T*U**2-
     . 60*L2T*S*T*U**2+120*lpol*L2T*S*T*U**2+40*NF*S*T*U**2+
     . 24*LMU*NF*S*T*U**2-80*lpol*NF*S*T*U**2-48*LMU*lpol*NF*S*T*U**2-
     . 12*LS*NF*S*T*U**2+24*lpol*LS*NF*S*T*U**2-12*LT*NF*S*T*U**2+
     . 24*lpol*LT*NF*S*T*U**2-108*PI**2*S*T*U**2+
     . 216*lpol*PI**2*S*T*U**2+378*T**2*U**2+594*LMU*T**2*U**2-
     . 756*lpol*T**2*U**2-1188*LMU*lpol*T**2*U**2-162*LS*T**2*U**2+
     . 324*lpol*LS*T**2*U**2-252*LS*LT*T**2*U**2+
     . 504*lpol*LS*LT*T**2*U**2-72*LS*LU*T**2*U**2+
     . 144*lpol*LS*LU*T**2*U**2+180*L2S*T**2*U**2-
     . 360*lpol*L2S*T**2*U**2-60*NF*T**2*U**2-36*LMU*NF*T**2*U**2+
     . 120*lpol*NF*T**2*U**2+72*LMU*lpol*NF*T**2*U**2+
     . 36*LS*NF*T**2*U**2-72*lpol*LS*NF*T**2*U**2+162*PI**2*T**2*U**2-
     . 324*lpol*PI**2*T**2*U**2
	temp=temp*2.d0/243.d0/(T**2*S**2)  
        elseif(jinst.eq.4)then
          temp=378*S**2*T**2+594*LMU*S**2*T**2-162*LU*S**2*T**2
     . -72*LS*LU*S**2*T**2-
     . 252*LT*LU*S**2*T**2+180*L2U*S**2*T**2-60*NF*S**2*T**2-
     . 36*LMU*NF*S**2*T**2+36*LU*NF*S**2*T**2+162*PI**2*S**2*T**2+
     . 378*T**4+594*LMU*T**4-756*lpol*T**4-1188*LMU*lpol*T**4-
     . 162*LU*T**4+324*lpol*LU*T**4-72*LS*LU*T**4+
     . 144*lpol*LS*LU*T**4-252*LT*LU*T**4+504*lpol*LT*LU*T**4+
     . 180*L2U*T**4-360*lpol*L2U*T**4-60*NF*T**4-36*LMU*NF*T**4+
     . 120*lpol*NF*T**4+72*LMU*lpol*NF*T**4+36*LU*NF*T**4-
     . 72*lpol*LU*NF*T**4+162*PI**2*T**4-324*lpol*PI**2*T**4-
     . 252*S**2*T*U-396*LMU*S**2*T*U+54*LT*S**2*T*U+
     . 120*LS*LT*S**2*T*U+54*LU*S**2*T*U+120*LS*LU*S**2*T*U-
     . 24*LT*LU*S**2*T*U-60*L2T*S**2*T*U-60*L2U*S**2*T*U+
     . 40*NF*S**2*T*U+24*LMU*NF*S**2*T*U-12*LT*NF*S**2*T*U-
     . 12*LU*NF*S**2*T*U-108*PI**2*S**2*T*U-120*LT*S*T**2*U+
     . 120*LU*S*T**2*U+36*LS*LU*S*T**2*U-72*lpol*LS*LU*S*T**2*U-
     . 132*LT*LU*S*T**2*U-18*L2S*S*T**2*U+36*lpol*L2S*S*T**2*U+
     . 66*L2T*S*T**2*U+48*L2U*S*T**2*U+36*lpol*L2U*S*T**2*U+
     . 48*PI**2*S*T**2*U+36*lpol*PI**2*S*T**2*U-36*LS*T**3*U+
     . 72*lpol*LS*T**3*U+36*LU*T**3*U-72*lpol*LU*T**3*U-
     . 36*LS*LU*T**3*U+72*lpol*LS*LU*T**3*U+126*LT*LU*T**3*U+
     . 18*L2S*T**3*U-36*lpol*L2S*T**3*U-63*L2T*T**3*U-45*L2U*T**3*U-
     . 36*lpol*L2U*T**3*U-45*PI**2*T**3*U-36*lpol*PI**2*T**3*U+
     . 378*S**2*U**2+594*LMU*S**2*U**2-162*LT*S**2*U**2-
     . 72*LS*LT*S**2*U**2-252*LT*LU*S**2*U**2+180*L2T*S**2*U**2-
     . 60*NF*S**2*U**2-36*LMU*NF*S**2*U**2+36*LT*NF*S**2*U**2+
     . 162*PI**2*S**2*U**2+120*LT*S*T*U**2+36*LS*LT*S*T*U**2-
     . 72*lpol*LS*LT*S*T*U**2-120*LU*S*T*U**2-132*LT*LU*S*T*U**2-
     . 18*L2S*S*T*U**2+36*lpol*L2S*S*T*U**2+48*L2T*S*T*U**2+
     . 36*lpol*L2T*S*T*U**2+66*L2U*S*T*U**2+48*PI**2*S*T*U**2+
     . 36*lpol*PI**2*S*T*U**2+12*LT*LU*T**2*U**2-6*L2T*T**2*U**2-
     . 6*L2U*T**2*U**2-6*PI**2*T**2*U**2-36*LS*T*U**3+
     . 72*lpol*LS*T*U**3+36*LT*T*U**3-72*lpol*LT*T*U**3-
     . 36*LS*LT*T*U**3+72*lpol*LS*LT*T*U**3+126*LT*LU*T*U**3+
     . 18*L2S*T*U**3-36*lpol*L2S*T*U**3-45*L2T*T*U**3-
     . 36*lpol*L2T*T*U**3-63*L2U*T*U**3-45*PI**2*T*U**3-
     . 36*lpol*PI**2*T*U**3+378*U**4+594*LMU*U**4-756*lpol*U**4-
     . 1188*LMU*lpol*U**4-162*LT*U**4+324*lpol*LT*U**4-72*LS*LT*U**4+
     . 144*lpol*LS*LT*U**4-252*LT*LU*U**4+504*lpol*LT*LU*U**4+
     . 180*L2T*U**4-360*lpol*L2T*U**4-60*NF*U**4-36*LMU*NF*U**4+
     . 120*lpol*NF*U**4+72*LMU*lpol*NF*U**4+36*LT*NF*U**4-
     . 72*lpol*LT*NF*U**4+162*PI**2*U**4-324*lpol*PI**2*U**4
	temp=temp*2.d0/243.d0/(T**2*U**2)  
      else
          write(6,*)'Error in xmatel_four_part: wrong jinst value'
          stop
        endif
      else 
        write(6,*)'Error in xmatel_four_part: wrong jproc value'
        stop
      endif
      x_loop_four=temp/(2*esq)
      return
      end


      function theta(flag)
c This function is the Heaviside function
      implicit real*8(a-z)
      logical flag
c
      if(flag)then
        tmp=1.d0
      else
        tmp=0.d0
      endif
      theta=tmp
      return
      end
 

C CERNLIB programs used by the heavy quark package.
C

      DOUBLE PRECISION FUNCTION DDILOG(X)

      DOUBLE PRECISION X,Y,T,S,A,PI3,PI6,ZERO,ONE,HALF,MALF,MONE,MTWO
      DOUBLE PRECISION C(0:18),H,ALFA,B0,B1,B2

      DATA ZERO /0.0D0/, ONE /1.0D0/
      DATA HALF /0.5D0/, MALF /-0.5D0/, MONE /-1.0D0/, MTWO /-2.0D0/
      DATA PI3 /3.28986 81336 96453D0/, PI6 /1.64493 40668 48226D0/

      DATA C( 0) / 0.42996 69356 08137 0D0/
      DATA C( 1) / 0.40975 98753 30771 1D0/
      DATA C( 2) /-0.01858 84366 50146 0D0/
      DATA C( 3) / 0.00145 75108 40622 7D0/
      DATA C( 4) /-0.00014 30418 44423 4D0/
      DATA C( 5) / 0.00001 58841 55418 8D0/
      DATA C( 6) /-0.00000 19078 49593 9D0/
      DATA C( 7) / 0.00000 02419 51808 5D0/
      DATA C( 8) /-0.00000 00319 33412 7D0/
      DATA C( 9) / 0.00000 00043 45450 6D0/
      DATA C(10) /-0.00000 00006 05784 8D0/
      DATA C(11) / 0.00000 00000 86121 0D0/
      DATA C(12) /-0.00000 00000 12443 3D0/
      DATA C(13) / 0.00000 00000 01822 6D0/
      DATA C(14) /-0.00000 00000 00270 1D0/
      DATA C(15) / 0.00000 00000 00040 4D0/
      DATA C(16) /-0.00000 00000 00006 1D0/
      DATA C(17) / 0.00000 00000 00000 9D0/
      DATA C(18) /-0.00000 00000 00000 1D0/

      IF(X .EQ. ONE) THEN
       DDILOG=PI6
       RETURN
      ELSE IF(X .EQ. MONE) THEN
       DDILOG=MALF*PI6
       RETURN
      END IF
      T=-X
      IF(T .LE. MTWO) THEN
       Y=MONE/(ONE+T)
       S=ONE
       A=-PI3+HALF*(LOG(-T)**2-LOG(ONE+ONE/T)**2)
      ELSE IF(T .LT. MONE) THEN
       Y=MONE-T
       S=MONE
       A=LOG(-T)
       A=-PI6+A*(A+LOG(ONE+ONE/T))
      ELSE IF(T .LE. MALF) THEN
       Y=(MONE-T)/T
       S=ONE
       A=LOG(-T)
       A=-PI6+A*(MALF*A+LOG(ONE+T))
      ELSE IF(T .LT. ZERO) THEN
       Y=-T/(ONE+T)
       S=MONE
       A=HALF*LOG(ONE+T)**2
      ELSE IF(T .LE. ONE) THEN
       Y=T
       S=ONE
       A=ZERO
      ELSE
       Y=ONE/T
       S=MONE
       A=PI6+HALF*LOG(T)**2
      END IF

      H=Y+Y-ONE
      ALFA=H+H
      B1=ZERO
      B2=ZERO
      DO 1 I = 18,0,-1
      B0=C(I)+ALFA*B1-B2
      B2=B1
    1 B1=B0
      DDILOG=-(S*(B0-H*B2)+A)
      RETURN
      END