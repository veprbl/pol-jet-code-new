// vim:et sw=3 sts=3

#include <Rivet/AnalysisHandler.hh>
#include <HepMC/GenEvent.h>

#define MUB_TO_PB 1e6 // microbarn to picobarn conversion

using namespace HepMC;

int parton_to_pdg(int iparton)
{
   switch(iparton) {
      case 1: // proton
         return 2212;
      case -1: // antiproton
         return -2212;
      case 4: // photon
         return 22;
      case 5: // electron
         return 11;
      default:
         throw "Invalid parton id";
   }
}

std::string run_name;
static Rivet::AnalysisHandler *rivet = NULL;

void rivetinit(const std::string &_run_name, const std::vector<std::string> &analyses)
{
   run_name = _run_name;
   rivet = new Rivet::AnalysisHandler();
   rivet->addAnalyses(analyses);
   if (rivet->analyses().size() < analyses.size()) {
      throw "one or more analyses were not found";
   }
}

extern "C" {

extern struct {
   int ih1;
   int ih2;
   int ndns1;
   int ndns2;
} pdf_;

extern struct {
   double sh;
} shadr_;

extern struct {
   double xkt_cm[3];
   double xeta_cm[3];
   double xphi_cm[3];
} cmkin_;

extern struct {
  int icont;
  double x1;
  double x2;
  int ireflex;
} contnx_;

void rivetshutdown_(const double &xsec)
{
   rivet->setCrossSection(xsec * MUB_TO_PB);
   rivet->finalize();
   rivet->writeData(run_name + ".yoda");
}

bool odd = false;

void rivetout_(const double &weight)
{
   const double E_CM = sqrt(shadr_.sh);
   const int BEAM1 = parton_to_pdg(pdf_.ih1);
   const int BEAM2 = parton_to_pdg(pdf_.ih2);

   double x1, x2;
   assert(contnx_.ireflex == odd);
   if (contnx_.ireflex) {
     x1 = contnx_.x2;
     x2 = contnx_.x1;
   } else {
     x1 = contnx_.x1;
     x2 = contnx_.x2;
   }
   assert(!std::isnan(x1));
   assert(!std::isnan(x2));

   if (odd) {
     contnx_.icont = -1;
     contnx_.x1 = NAN;
     contnx_.x2 = NAN;
   }
   odd = !odd;

   if (weight == 0) return;

   GenEvent *evt = new GenEvent();
   evt->weights().push_back(weight);
   HepMC::PdfInfo pdf_info(0, 0, x1, x2, NAN, NAN, NAN);
   evt->set_pdf_info(pdf_info);

   evt->use_units(HepMC::Units::GEV, HepMC::Units::MM);
   GenVertex* v = new GenVertex();
   GenParticle* p1 = new GenParticle(FourVector(0, 0, +E_CM/2, E_CM/2), BEAM1, 3);
   GenParticle* p2 = new GenParticle(FourVector(0, 0, -E_CM/2, E_CM/2), BEAM2, 3);
   v->add_particle_in(p1);
   v->add_particle_in(p2);
   evt->set_beam_particles(p1, p2);
   for(int i = 0; i < 3; i++)
   {
      // absence of particle is marked by kt=0
      // gap may happen at any index
      if (cmkin_.xkt_cm[i] == 0) continue;

      double p_x = cmkin_.xkt_cm[i] * cos(cmkin_.xphi_cm[i]);
      double p_y = cmkin_.xkt_cm[i] * sin(cmkin_.xphi_cm[i]);
      double p_z = cmkin_.xkt_cm[i] * sinh(cmkin_.xeta_cm[i]);
      double p = cmkin_.xkt_cm[i] * cosh(cmkin_.xeta_cm[i]);
      GenParticle* po = new GenParticle(FourVector(p_x, p_y, p_z, p), 21, 1);
      v->add_particle_out(po);
   }
   evt->set_signal_process_vertex(v);

   rivet->analyze(*evt);
   delete evt;
}

}
