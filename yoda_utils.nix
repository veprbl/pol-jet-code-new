with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "yoda-utils";
  src = fetchgit {
    url = "git@github.com:veprbl/yoda-utils.git";
    rev = "3fd393df265c90f9db06c6a99da63c1b6595fdfe";
    sha256 = "1rg49vl0fv40y791jk272hizflxb5hci5mkcjsvxarlpin8avflp";
  };
  propagatedBuildInputs = [ python2 yoda ];
  phases = [ "unpackPhase" "installPhase" "fixupPhase" ];
  installPhase = ''
    for i in *; do
      install -Dm755 $i $out/bin/''${i%.*}
    done
  '';
}
