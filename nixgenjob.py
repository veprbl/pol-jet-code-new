#!/usr/bin/env python

import argparse
import random

parser = argparse.ArgumentParser()
parser.add_argument("--num-jobs", type=int, default=1000,
                    help="count of condor jobs")
parser.add_argument("--seed", type=int, default=None,
                    help="random seed")
parser.add_argument("--executable", type=str, required=True,
                    help="poljet executable")
parser.add_argument("params", type=str, nargs='*',
                    help="parameters passed to the code")
args = parser.parse_args()

if args.seed is not None:
	random.seed(args.seed)

print """Universe = vanilla
Executable = {executable}
Log = job.log
Notification = Never
GetEnv = True
+Job_Type = "cas"
Priority = 0
+Experiment = "star"\
""".format(executable=args.executable)
for i in range(args.num_jobs):
    run_name = "j%04i" % i
    params = [
        ("run-name", run_name),
        ("seed", random.randint(0, 2147483647)),
        ("num-iterations", 10000000),
        ]
    params = " ".join(["--%s=%s" % (p,v) for p,v in params] + args.params)
    print """

Arguments = {params}
Output = {run_name}.out
Error  = {run_name}.err

should_transfer_files = YES
when_to_transfer_output = ON_EXIT
transfer_output_files = {run_name}.yoda

Queue""".format(run_name=run_name, params=params)
