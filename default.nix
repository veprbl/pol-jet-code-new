with import <nixpkgs> {};

{
  rivet_ana_name,
  sqrts,
  etmin,
  pdf_name,
  member ? 0,
  num_jobs ? "1000",
  order,
  initial_state ? null,
  xren ? "1.0",
  xfac ? "1.0",
}:

let
  is_polarized = {
    "CT10nlo" = "0";
    "NNPDF23_nlo_as_0119" = "0";
    "NNPDFpol10_100" = "1";
    "NNPDFpol11_100" = "1";
    "DSSV_2014_NLO" = "1";
    "MSTW2008nlo68cl" = "0";
  };
  rivet_ana = import ./rivet_ana.nix { inherit rivet_ana_name; };
  poljet = stdenv.mkDerivation {
    name = "poljet";
    srcs = [
./CMakeLists.txt
./cmdline.cc
./DSSV_GLUON_UPDATE.f
./pdf.f
./poljetcrs.f
./poljetdiff.f
./prog_top.f
./rivetout.cc
./scales.cc
./user.f
];
    unpackPhase = ''
      for file in $srcs; do
        stripHash "$file"
        ln -s $file $strippedName
      done
    '';
    buildInputs = [
      cmake
      boost
      hepmc
      lhapdf
      gfortran
      rivet
    ];
  };
in
stdenv.mkDerivation rec {
  name = "${rivet_ana.name}-${pdf_name}-m${toString member}-j${num_jobs}-${order}${if builtins.isNull initial_state then "" else "-is${initial_state}"}";
  srcs = [
    ./nixgenjob.py
  ];
  phases = [
    "unpackPhase"
    "buildPhase"
  ];
  unpackPhase = ''
    for file in $srcs; do
      stripHash "$file"
      ln -s $file $strippedName
    done
  '';

  buildInputs = [
    poljet
    rivet_ana
    python2
    python2Packages.argparse
    yoda
  ] ++ lib.optional (pdf_name != "DSSV_2014_NLO") lhapdf.pdf_sets.${pdf_name};

  polarized = is_polarized.${pdf_name};
  passthru = {
    inherit pdf_name;
  };

  buildPhase = ''
    ./nixgenjob.py --seed=0 --num-jobs=${num_jobs} --executable=${poljet}/r.e -- \
      -a ${rivet_ana.name} \
      --polarized ${polarized} \
      --sqrts ${sqrts} \
      --etmin ${etmin} \
      ${if pdf_name == "DSSV_2014_NLO" then "--pdf-backend=51 " else ""}--lhapdf-pdfname ${pdf_name} \
      --lhapdf-member ${toString member} \
      --order ${order} \
      --xren ${xren} \
      --xfac ${xfac} \
      ${if builtins.isNull initial_state then "" else "--initial-state ${initial_state}"} \
      > job
    sleep `expr $RANDOM % 600`
    CLUSTER_ID=`/usr/bin/condor_submit -terse job | cut -d "." -f 1`
    echo Spawned job $CLUSTER_ID
    trap "/usr/bin/condor_rm $CLUSTER_ID" 1 2 3 15
    /usr/bin/condor_wait job.log

    mkdir $out
    yodamerge -o $out/result.yoda j*.yoda
    cp *.out *.err $out/
    for yodafile in *.yoda; do
      xz --best --stdout $yodafile > $out/$yodafile.xz
    done
  '';
}
