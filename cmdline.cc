// vim:et sw=3 sts=3

#include <cstdlib>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

extern "C" {

extern struct {
   int num;
   int num2; // not used
} seed_;

void poljetdiff_main_();
void prntsf_();
void setpar_();
void x_inst_sum_(int &iinst);

extern char runstr_[80];

extern struct {
   int ih1;
   int ih2;
   int ndns1;
   int ndns2;
} pdf_;

extern struct {
   int lpol;
} polarized_;

extern int nl_;
extern int iproc_;
extern int iinst_;

extern struct {
   int min_proc;
   int max_proc;
} jprocess_;

extern struct {
   int ncl;
} num_iterations_;

extern struct {
   int lhapdf_member;
   char lhapdf_pdfname[25];
} pdfset_;

extern struct {
   double ecm;
   double sclr;
   double sclf;
   double xsc_min;
   int isnlo;
} runparams_;

}

void rivetinit(const std::string &_run_name, const std::vector<std::string> &analyses);

int main(int argc, char **argv)
{
   po::options_description desc("Allowed options");
   desc.add_options()
      ("help,h", "print help")
      ("run-name", po::value<std::string>()->default_value("Rivet"), "run name")
      ("pdf-backend", po::value<int>()->default_value(50), "specify -1 to display possible values")
      ("lhapdf-pdfname", po::value<std::string>()->default_value("CT10nlo"), "")
      ("lhapdf-member", po::value<int>()->default_value(0), "")
      ("sqrts", po::value<double>()->required(), "sqrt(s) [GeV]")
      ("polarized", po::value<int>()->required(), "0=unpolarized, 1=polarized")
      ("num-flavours", po::value<int>()->default_value(5), "number of flavours [3-5]")
      ("beam1", po::value<int>()->default_value(1), "beam 1 type [1=p, -1=pbar]")
      ("beam2", po::value<int>()->default_value(1), "beam 2 type")
      ("subprocess", po::value<int>()->default_value(0), "0=all, 1=5g, 2=3g2q, 3=1g2q2Q, 4=1g4q")
      ("initial-state", po::value<int>()->default_value(0), "0=all, 1=gg, 2=qg, 3=qqbar, 4=qq, 5=qQbar, 6=qQ, 7=qqbar+qq+qQbar+qQ, -1=exclude gg...")
      ("num-iterations", po::value<int>()->default_value(10000), "number of iterations")
      ("xren", po::value<double>()->default_value(1.), "renormalization scale")
      ("xfac", po::value<double>()->default_value(1.), "factorization scale")
      ("etmin", po::value<double>()->required(), "minimal transverse energy [GeV]")
      ("order", po::value<std::string>()->default_value("NLO"), "order of calculation [LO,NLO]")
      ("seed", po::value<int>()->default_value(1), "random seed")
      ("analysis,a", po::value< std::vector<std::string> >()->required(), "Rivet analyses to use")
      ;

   try {
      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);
      po::notify(vm);

      if (vm.count("help"))
      {
         std::cout << desc << std::endl;
         return EXIT_SUCCESS;
      }
      if (vm["pdf-backend"].as<int>() < 0)
      {
         prntsf_();
         return EXIT_SUCCESS;
      }

      std::string run_name = vm["run-name"].as<std::string>();
      if (run_name.size() > 20)
      {
         throw "run name is too long";
      }
      rivetinit(run_name, vm["analysis"].as< std::vector<std::string> >());
      memset(runstr_, ' ', sizeof(runstr_));
      memcpy(runstr_, run_name.c_str(), run_name.size());

      pdf_.ndns1 = vm["pdf-backend"].as<int>();
      pdf_.ndns2 = vm["pdf-backend"].as<int>();

      polarized_.lpol = vm["polarized"].as<int>();
      if ((polarized_.lpol != 0) && (polarized_.lpol != 1)) {
         throw "invalid value in --polarized option";
      }
      nl_ = vm["num-flavours"].as<int>();
      if ((nl_ < 3) || (nl_ > 5)) {
         throw "invalid number of flavours";
      }
      setpar_();

      pdf_.ih1 = vm["beam1"].as<int>();
      pdf_.ih2 = vm["beam2"].as<int>();
      jprocess_.min_proc = 1;
      jprocess_.max_proc = 4;
      iproc_ = vm["subprocess"].as<int>();
      if ((iproc_ < 0) || (iproc_ > 5)) {
         throw "invalid subprocess";
      }
      if (iproc_ > 0) {
         jprocess_.min_proc = iproc_;
         jprocess_.max_proc = iproc_;
      }

      iinst_ = vm["initial-state"].as<int>();
      if ((iinst_ < -6) || (iinst_ > 7)) {
         throw "invalid initial state";
      }
      x_inst_sum_(iinst_);

      num_iterations_.ncl = vm["num-iterations"].as<int>();
      if (num_iterations_.ncl < 0) {
          throw "invalid value in --num_iterations";
      }

      std::string lhapdf_pdfname = vm["lhapdf-pdfname"].as<std::string>();
      memset(pdfset_.lhapdf_pdfname, ' ', sizeof(pdfset_.lhapdf_pdfname));
      memcpy(pdfset_.lhapdf_pdfname, lhapdf_pdfname.c_str(), lhapdf_pdfname.size());
      pdfset_.lhapdf_member = vm["lhapdf-member"].as<int>();

      runparams_.ecm = vm["sqrts"].as<double>();
      runparams_.sclr = vm["xren"].as<double>();
      runparams_.sclf = vm["xfac"].as<double>();
      runparams_.xsc_min = vm["etmin"].as<double>();

      if (vm["order"].as<std::string>() == "NLO") {
         runparams_.isnlo = 1;
      } else if (vm["order"].as<std::string>() == "LO") {
         runparams_.isnlo = 0;
      } else {
         throw "invalid calculation order";
      }

      seed_.num = vm["seed"].as<int>();
      if (seed_.num <= 0) {
         throw "seed value must be above zero";
      }

      poljetdiff_main_();
      return EXIT_SUCCESS;
   } catch (std::exception &e) {
      std::cerr << "Error: " << e.what()
                << std::endl
                << std::endl
                << desc
                << std::endl;
      return EXIT_FAILURE;
   } catch (const char *str) {
      std::cerr << "Error: " << str
                << std::endl
                << std::endl
                << desc
                << std::endl
                ;
      return EXIT_FAILURE;
   }
}
