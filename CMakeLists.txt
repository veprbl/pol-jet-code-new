cmake_minimum_required (VERSION 2.6)
project (pol-jet-code)

enable_language (Fortran)

# workaround for
# http://stackoverflow.com/questions/9948375/cmake-find-package-succeeds-but-returns-wrong-path
set(Boost_NO_BOOST_CMAKE ON)
find_package(Boost 1.41 REQUIRED COMPONENTS program_options)

find_program (LHAPDF_CONFIG_EXECUTABLE lhapdf-config)
if (NOT LHAPDF_CONFIG_EXECUTABLE)
	message (FATAL_ERROR "lhapdf-config not found")
endif ()

exec_program (
	${LHAPDF_CONFIG_EXECUTABLE}
	ARGS --cppflags
	OUTPUT_VARIABLE LHAPDF_CPP_FLAGS
	)
string(REGEX REPLACE "(^| )-I" "\\1" LHAPDF_INCLUDE_DIR "${LHAPDF_CPP_FLAGS}")

exec_program (
	${LHAPDF_CONFIG_EXECUTABLE}
	ARGS --libs
	OUTPUT_VARIABLE LHAPDF_LIBRARIES
	)

find_library (
	HepMC_LIBRARY
	NAMES HepMC
	)
if (NOT HepMC_LIBRARY)
	message (FATAL_ERROR "HEPMC library not found")
endif ()

get_filename_component (HepMC_LIBRARY_DIR ${HepMC_LIBRARY} PATH)
find_path (
	HepMC_INCLUDE_DIR
	GenEvent.h
	HINTS
	"${HepMC_LIBRARY_DIR}/../include/HepMC"
	)
get_filename_component (HepMC_INCLUDE_DIR ${HepMC_INCLUDE_DIR} PATH)

find_program (Rivet_CONFIG_EXECUTABLE rivet-config)
if (NOT Rivet_CONFIG_EXECUTABLE)
	message (FATAL_ERROR "rivet-config not found")
endif ()

exec_program (
	${Rivet_CONFIG_EXECUTABLE}
	ARGS --includedir
	OUTPUT_VARIABLE Rivet_INCLUDE_DIR
	)

exec_program (
	${Rivet_CONFIG_EXECUTABLE}
	ARGS --ldflags --libs
	OUTPUT_VARIABLE Rivet_LIBRARIES
	)

set (CMAKE_CXX_FLAGS "-std=c++11")
set (CMAKE_Fortran_FLAGS "-fno-automatic")

include_directories (${Boost_INCLUDE_DIRS} ${LHAPDF_INCLUDE_DIR} ${HepMC_INCLUDE_DIR} ${Rivet_INCLUDE_DIR})

add_executable (
	r.e
	prog_top.f
	pdf.f
	poljetcrs.f
	poljetdiff.f
	user.f
	DSSV_GLUON_UPDATE.f
	rivetout.cc
	cmdline.cc
	scales.cc
	)

target_link_libraries (r.e ${Boost_LIBRARIES} ${LHAPDF_LIBRARIES} ${HepMC_LIBRARY} ${Rivet_LIBRARIES} -pthread)

install(TARGETS r.e RUNTIME DESTINATION .)
