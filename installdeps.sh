#!/bin/sh
# vim: set noexpandtab:

set -x
set -e

MYTMP=/tmp/nix-boot-`whoami`
INSTALL_PREFIX=$PWD/deps
NUM_THREADS=4

rm -rf $MYTMP
mkdir $MYTMP
pushd $MYTMP 1>/dev/null 2>/dev/null

wget http://www.hepforge.org/archive/yoda/YODA-1.7.0.tar.bz2
wget http://www.hepforge.org/archive/rivet/Rivet-2.6.0.tar.bz2
wget http://hepmc.web.cern.ch/hepmc/releases/hepmc2.06.09.tgz
wget http://fastjet.fr/repo/fastjet-3.3.0.tar.gz
wget http://www.hepforge.org/archive/lhapdf/LHAPDF-6.2.1.tar.gz
wget http://ftp.math.utah.edu/pub/tex/historic/systems/texlive/2019/install-tl-unx.tar.gz

tar zxf LHAPDF-6.2.1.tar.gz
tar jxf YODA-1.7.0.tar.bz2
tar jxf Rivet-2.6.0.tar.bz2
tar zxf hepmc2.06.09.tgz
tar zxf fastjet-3.3.0.tar.gz
tar zxf install-tl-unx.tar.gz

function mmi () {
        make -j $NUM_THREADS 1> /dev/null
        make install 1> /dev/null
}

export PATH=$INSTALL_PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$INSTALL_PREFIX/lib64:$INSTALL_PREFIX/lib:$LD_LIBRARY_PATH
unset CONFIG_SITE

pushd hepmc2.06.09 1>/dev/null 2>/dev/null
./configure --prefix=$INSTALL_PREFIX --with-momentum=GEV --with-length=MM 1>/dev/null 2>/dev/null
mmi
popd 1>/dev/null 2>/dev/null

pushd fastjet-3.3.0 1>/dev/null 2>/dev/null
./configure --prefix=$INSTALL_PREFIX --enable-allcxxplugins 1>/dev/null 2>/dev/null
mmi
popd 1>/dev/null 2>/dev/null

pushd YODA-1.7.0 1>/dev/null 2>/dev/null
./configure --prefix=$INSTALL_PREFIX 1>/dev/null 2>/dev/null
mmi
popd 1>/dev/null 2>/dev/null

pushd Rivet-2.6.0 1>/dev/null 2>/dev/null
./configure --prefix=$INSTALL_PREFIX --disable-analyses 1>/dev/null 2>/dev/null
mmi
popd 1>/dev/null 2>/dev/null

pushd LHAPDF-6.2.1 1>/dev/null 2>/dev/null
./configure --prefix=$INSTALL_PREFIX 1>/dev/null 2>/dev/null
mmi
popd 1>/dev/null 2>/dev/null

pushd install-tl-20190410 1>/dev/null 2>/dev/null
export TEXLIVE_INSTALL_PREFIX=$INSTALL_PREFIX
export TEXLIVE_INSTALL_TEXDIR=$INSTALL_PREFIX/2019
TEXLIVE_ARCH=${TEXLIVE_ARCH:-x86_64-linux}
cat >./texlive.profile <<EOF
selected_scheme scheme-basic
TEXDIR $TEXLIVE_INSTALL_TEXDIR
TEXMFCONFIG \$TEXMFSYSCONFIG
TEXMFHOME \$TEXMFLOCAL
TEXMFLOCAL $TEXLIVE_INSTALL_PREFIX/texmf-local
TEXMFSYSCONFIG $TEXLIVE_INSTALL_TEXDIR/texmf-config
TEXMFSYSVAR $TEXLIVE_INSTALL_TEXDIR/texmf-var
TEXMFVAR \$TEXMFSYSVAR
binary_$TEXLIVE_ARCH 1
instopt_adjustpath 1
instopt_adjustrepo 1
instopt_letter 0
instopt_portable 1
instopt_write18_restricted 1
tlpdbopt_autobackup 0
tlpdbopt_backupdir tlpkg/backups
tlpdbopt_create_formats 1
tlpdbopt_desktop_integration 0
tlpdbopt_file_assocs 0
tlpdbopt_generate_updmap 0
tlpdbopt_install_docfiles 0
tlpdbopt_install_srcfiles 0
tlpdbopt_post_code 1
tlpdbopt_sys_bin $TEXLIVE_INSTALL_PREFIX/bin
tlpdbopt_sys_info $TEXLIVE_INSTALL_PREFIX/share/info
tlpdbopt_sys_man $TEXLIVE_INSTALL_PREFIX/share/man
tlpdbopt_w32_multi_user 1
EOF
# gpg keys for texlive-2019 have expired so -no-verify-downloads
./install-tl \
	-no-verify-downloads \
	-repository http://ftp.math.utah.edu/pub/tex/historic/systems/texlive/2019/tlnet-final/ \
	-profile texlive.profile
export PATH=$TEXLIVE_INSTALL_TEXDIR/bin/$TEXLIVE_ARCH:$PATH
tlmgr install \
	collection-pstricks \
	collection-fontsrecommended \
	l3kernel \
	l3packages \
	mathastext \
	pgf \
	relsize \
	sfmath \
	siunitx \
	xcolor \
	xkeyval \
	xstring
popd 1>/dev/null 2>/dev/null

popd 1>/dev/null 2>/dev/null

cat >"$INSTALL_PREFIX/env.csh" <<EOF
setenv PATH "$INSTALL_PREFIX/bin:$TEXLIVE_INSTALL_TEXDIR/bin/$TEXLIVE_ARCH:\$PATH"
setenv LD_LIBRARY_PATH "$INSTALL_PREFIX/lib:\$LD_LIBRARY_PATH"
setenv PYTHONPATH "$INSTALL_PREFIX/lib64/python2.6/site-packages:\$PYTHONPATH"
setenv PYTHONPATH "$INSTALL_PREFIX/lib64/python2.7/site-packages:\$PYTHONPATH"

setenv TEXMFHOME "$INSTALL_PREFIX/share/Rivet/texmf:\$TEXMFHOME"
setenv HOMETEXMF "$INSTALL_PREFIX/share/Rivet/texmf:\$HOMETEXMF"
setenv TEXMFCNF "$INSTALL_PREFIX/share/Rivet/texmf/cnf:\$TEXMFCNF"
setenv TEXINPUTS "$INSTALL_PREFIX/share/Rivet/texmf/tex/:\$TEXINPUTS"
setenv LATEXINPUTS "$INSTALL_PREFIX/share/Rivet/texmf/tex/:\$LATEXINPUTS"

setenv CMAKE_PREFIX_PATH "$INSTALL_PREFIX"
EOF

cat >"$INSTALL_PREFIX/env.sh" <<EOF
export PATH="$INSTALL_PREFIX/bin:$TEXLIVE_INSTALL_TEXDIR/bin/$TEXLIVE_ARCH:\$PATH"
export LD_LIBRARY_PATH="$INSTALL_PREFIX/lib:\$LD_LIBRARY_PATH"
export PYTHONPATH="$INSTALL_PREFIX/lib64/python2.6/site-packages:\$PYTHONPATH"
export PYTHONPATH="$INSTALL_PREFIX/lib64/python2.7/site-packages:\$PYTHONPATH"

export TEXMFHOME="$INSTALL_PREFIX/share/Rivet/texmf:\$TEXMFHOME"
export HOMETEXMF="$INSTALL_PREFIX/share/Rivet/texmf:\$HOMETEXMF"
export TEXMFCNF="$INSTALL_PREFIX/share/Rivet/texmf/cnf:\$TEXMFCNF"
export TEXINPUTS="$INSTALL_PREFIX/share/Rivet/texmf/tex/:\$TEXINPUTS"
export LATEXINPUTS="$INSTALL_PREFIX/share/Rivet/texmf/tex/:\$LATEXINPUTS"

export CMAKE_PREFIX_PATH="$INSTALL_PREFIX"
EOF
