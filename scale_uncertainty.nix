with import <nixpkgs> {};

{fac}:

let
  yoda_utils = import ./yoda_utils.nix;
in
  stdenv.mkDerivation {
    name = "${(fac "1.0" "1.0").name}-scale-uncertainty";
    buildInputs = [
      yoda_utils
    ];
    phases = [ "buildPhase" ];
    buildPhase = ''
      mkdir $out
      yoda-envelope \
        -o $out/result.yoda \
        -c ${fac "1.0" "1.0"}/result.yoda \
        ${fac "2.0" "1.0"}/result.yoda \
        ${fac "0.5" "1.0"}/result.yoda \
        ${fac "1.0" "2.0"}/result.yoda \
        ${fac "1.0" "0.5"}/result.yoda \
        ${fac "2.0" "2.0"}/result.yoda \
        ${fac "0.5" "0.5"}/result.yoda
    '';
  }
