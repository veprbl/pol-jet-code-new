with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "pol-jet-code-shell";
  buildInputs = [
    boost
    cmake
    gfortran
    hepmc
    lhapdf
    rivet
  ];
}
