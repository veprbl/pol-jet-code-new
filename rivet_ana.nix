with import <nixpkgs> {};

{ rivet_ana_name }:

stdenv.mkDerivation {
  name = rivet_ana_name;
  srcs = map (p: /. + builtins.toPath p) [
"/direct/star+u/veprbl/star-rivet-analyses/${rivet_ana_name}/${rivet_ana_name}.cc"
"/direct/star+u/veprbl/star-rivet-analyses/${rivet_ana_name}/${rivet_ana_name}.plot"
"/direct/star+u/veprbl/star-rivet-analyses/${rivet_ana_name}/${rivet_ana_name}.info"
];
  buildInputs = [ rivet ];

  phases = [
    "unpackPhase"
    "buildPhase"
    "installPhase"
    "fixupPhase"
  ];
  unpackPhase = ''
    for file in $srcs; do
      stripHash "$file"
      ln -s $file $strippedName
    done
  '';
  buildPhase = ''
    rivet-buildplugin ${rivet_ana_name}.cc
  '';
  installPhase = ''
    mkdir $out
    for i in *; do
      install -Dm755 $i $out/$i
    done
  '';

  setupHook = ./ana-setup-hook.sh;
}
