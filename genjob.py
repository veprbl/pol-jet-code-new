#!/usr/bin/env python

import argparse
import random

parser = argparse.ArgumentParser()
parser.add_argument("--num_jobs", type=int, default=1000,
                    help="count of condor jobs")
parser.add_argument("--seed", type=int, default=None,
                    help="random seed")
args = parser.parse_args()

if args.seed is not None:
	random.seed(args.seed)

class PDF(object):
    name = None
    member = -1
    def __str__(self):
        return str(self.id)

class LHAPDF(PDF):
    def __init__(self, name, member):
        self.id = 50
        self.name = name
        self.member = member
        self.polarized = name.startswith("NNPDFpol") or name.startswith("DSSV")

    def __str__(self):
        return "{id}\n{name}\n{member}".format(**self.__dict__)

class CTEQ6M(PDF):
    polarized = False
    id = 43

class CTEQ6L(PDF):
    polarized = False
    id = 45

class MRST2002NLO(PDF):
    polarized = False
    id = 46

class DSSVNEWFIT(PDF):
    polarized = True
    id = 51

class DSSV2008NLO(PDF):
    polarized = True
    id = 69

def mkjob(prefix, pdf, **kwargs):
    print """Universe = vanilla
Executable = ./r.e
Log = job.log
Notification = Never
GetEnv = True
+Job_Type = "cas"
Priority = 0
+Experiment = "star"\
"""
    for i in range(args.num_jobs):
        run_name = "%sj%04i" % (prefix, i)
        params = [
            ("run-name", run_name),
            ("pdf-backend", pdf.id),
            ("polarized", int(pdf.polarized)),
            ("lhapdf-pdfname", pdf.name),
            ("lhapdf-member", pdf.member),
            ("seed", random.randint(0, 2147483647)),
            ("analysis", "STAR_DIJET"),
            ("num-iterations", 10000000),
            ("sqrts", 200),
            ("etmin", 14),
            ]
        params += map(lambda (k,v): (k.replace("_", "-"), v), kwargs.items())
        params = " ".join(["--%s=%s" % (p,v) for p,v in params])
        print """

Arguments = {params}
Output = {run_name}.out
#Error  = {run_name}.err
Queue""".format(run_name=run_name, params=params)

if __name__ == '__main__':
    # Example usage:
    #mkjob("nnpdfpol10", LHAPDF("NNPDFpol10_100", 0))
    #mkjob("nnpdfpol11", LHAPDF("NNPDFpol11_100", 0))
    #mkjob("nnpdf23", LHAPDF("NNPDF23_nlo_as_0119", 0))
    #mkjob("mrst2002nlo", MRST2002NLO())
    #mkjob("mstw2008nlo", LHAPDF("MSTW2008nlo68cl", 0))
    #mkjob("dssvnew", DSSVNEWFIT())
    #mkjob("dssv2008nlo", DSSV2008NLO())
    #for member in range(1,100+1):
    #    mkjob("np11_%i" % member, LHAPDF("NNPDFpol11_100", member))
    #for r, f in [(2.0, 1.0), (0.5, 1.0), (1.0, 2.0), (1.0, 0.5), (2.0, 2.0), (0.5, 0.5)]:
    #    mkjob("n23_%1.1f_%1.1f" % (r, f), LHAPDF("NNPDF23_nlo_as_0119", 0), xren=r, xfac=f)
    #    mkjob("n11_%1.1f_%1.1f" % (r, f), LHAPDF("NNPDFpol11_100", 0), xren=r, xfac=f)
