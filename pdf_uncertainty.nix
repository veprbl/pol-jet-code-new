with import <nixpkgs> {};

{member, num_members}:

let
  yoda_utils = import ./yoda_utils.nix;
  member_list = builtins.genList (x: x+1) num_members;
  member_file = m: "${member m}/result.yoda";
  member_files = map member_file member_list;
  err_vec_type = {
    "CT10nlo" = "hessian";
    "NNPDF23_nlo_as_0119" = "replica";
    "NNPDFpol10_100" = "replica";
    "NNPDFpol11_100" = "replica";
  };
in
  stdenv.mkDerivation {
    name = "${(member 0).name}-pdf-uncertainty";
    buildInputs = [
      yoda_utils
      python2Packages.numpy
    ];
    phases = [ "buildPhase" ];
    buildPhase = ''
      mkdir $out
      yoda-uncertainty \
        -o $out/result.yoda \
        -c ${member_file 0} \
        -t ${err_vec_type.${(member 0).pol_pdf_name}} \
        ${builtins.foldl' (x: y: x + " " + y) "" member_files}
    '';
  }
