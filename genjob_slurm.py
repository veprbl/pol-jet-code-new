#!/usr/bin/env python3

import argparse
import random
import math
import os

WORKDIR = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser()
parser.add_argument("--num_iterations", type=str, default="10M",
                    help="iterations to perform in each job")
parser.add_argument("--job_name", type=str, default="unnamed",
                    help="name of the job (acts as a filename prefix as well)")
parser.add_argument("--num_jobs", type=int, default=1000,
                    help="number of jobs to run")
parser.add_argument("--seed", type=int, default=None,
                    help="random seed")
parser.add_argument("--slurm_cpus_per_task", type=int, default=1,
                    help="number of threads per node")
parser.add_argument("--order", type=str, default="LO",
                    help="pQCD order")
args = parser.parse_args()

if args.num_iterations[-1] == "G":
	# billions
	args.num_iterations = int(args.num_iterations[:-1]) * 1000000000
elif args.num_iterations[-1] == "M":
	# millions
	args.num_iterations = int(args.num_iterations[:-1]) * 1000000
else:
	args.num_iterations = int(args.num_iterations)

if args.seed is not None:
	random.seed(args.seed)

print(f"""\
#!/bin/bash

#SBATCH -J {args.job_name}
#SBATCH -p general
#SBATCH -o output_%j.txt
#SBATCH -e output_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=18126711323@tmomail.net
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task={args.slurm_cpus_per_task}
#SBATCH --time=4-00:00:00

sh -c 'while true; do pstree '$$'; sleep 300; done' &
mkdir -p log time

(
""")

num_job_digits = math.ceil(math.log10(max(2, args.num_jobs - 1)))
for i in range(args.num_jobs):
    task_name = f"{args.job_name}_{i:0{num_job_digits}}"
    seed = random.randint(0, 2147483647)
    print(f"printf \"command time -vo time/{task_name} {WORKDIR}/build/r.e --order={args.order} --run-name={task_name} --seed={seed} --pdf-backend=50 --polarized=0 --lhapdf-pdfname=CT10 --lhapdf-member=0 --num-iterations={args.num_iterations} --sqrts=200 --etmin=14 --analysis=STAR_2018_I1674714 2>&1 > log/{task_name}.log\\0\"")

print(f"""
) | xargs --max-procs={args.slurm_cpus_per_task} --no-run-if-empty -0 -n1 sh -xec
""")
